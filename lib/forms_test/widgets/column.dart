// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import './row.dart';

class MyColumn extends StatelessWidget {
  const MyColumn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            MyRow(firstFlex: 1, secondFlex: 3),
            MyRow(firstFlex: 2, secondFlex: 1),
          ],
        ),
      ),
    );
  }
}
