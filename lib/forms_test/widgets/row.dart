import 'package:flutter/material.dart';
import './cell.dart';

class MyRow extends StatelessWidget {
  const MyRow({
    Key? key,
    required this.firstFlex,
    required this.secondFlex,
  }) : super(key: key);

  final int firstFlex;
  final int secondFlex;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 600,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
      ),
      child: Row(
        children: [
          MyCell(flex: firstFlex),
          MyCell(flex: secondFlex),
        ],
      ),
    );
  }
}
