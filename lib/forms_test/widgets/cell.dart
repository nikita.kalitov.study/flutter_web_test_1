import 'package:flutter/material.dart';

class MyCell extends StatelessWidget {
  const MyCell({Key? key, required this.flex}) : super(key: key);

  final int flex;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: flex,
      child: Container(
        decoration: const BoxDecoration(
          border: Border.symmetric(
            vertical: BorderSide(color: Colors.black),
          ),
        ),
        child: const Padding(
          padding: EdgeInsets.all(8.0),
          child: TextField(
            maxLines: null,
          ),
        ),
      ),
    );
  }
}
