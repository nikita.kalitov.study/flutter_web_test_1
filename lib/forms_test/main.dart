import 'package:flutter/material.dart';
import './screens/home_screen.dart';

void main() {
  runApp(returnMaterialApp());
}

MaterialApp returnMaterialApp() {
  return const MaterialApp(
    home: HomeScreen(),
  );
}
