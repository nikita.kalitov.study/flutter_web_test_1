// ignore_for_file: prefer_final_fields

import './classes/database.dart';

class DatabaseProvider {
  bool _isAuthorized = Database().isAuthorized;

  void authStatusOut() {
    _isAuthorized = false;
  }

  void authStatusIn() {
    _isAuthorized = true;
  }

  bool checkIfAuthorized() {
    return _isAuthorized;
  }
}

DatabaseProvider provider = DatabaseProvider();
