part of 'app_bloc.dart';

enum AppStates {
  loading,
  unauthorized,
  authorized,
}

class AppState extends Equatable {
  const AppState({required this.isLoading, required this.isAuthorized});

  final bool isLoading;
  final bool isAuthorized;

  @override
  List<Object> get props => [isLoading, isAuthorized];

  AppState copyWith(bool? isLoading, bool? isAuthorized) {
    return AppState(
      isLoading: isLoading ?? this.isLoading,
      isAuthorized: isAuthorized ?? this.isAuthorized,
    );
  }
}

// class AppInitial extends AppState {}

// class AppLoading extends AppState {}

// class AppUnauthorized extends AppState {}

// class AppAuthorized extends AppState {}
