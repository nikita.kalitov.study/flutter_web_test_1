// ignore_for_file: prefer_const_constructors

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import '../provider.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc()
      : super(AppState(
            isLoading: true, isAuthorized: provider.checkIfAuthorized())) {
    on<AppEvent>((event, emit) {
      // TODO: implement event handler
    });
  }
}
