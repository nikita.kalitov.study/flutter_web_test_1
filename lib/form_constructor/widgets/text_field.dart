import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  const MyTextField({Key? key}) : super(key: key);

  final String innerHint = 'Подсказка внутри';
  final String bottomHint = 'Подсказка снаружи';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              border: Border.all(color: Colors.black),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                minLines: 2,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: innerHint,
                ),
                maxLines: null,
              ),
            ),
          ),
          Text(bottomHint),
        ],
      ),
    );
  }
}
