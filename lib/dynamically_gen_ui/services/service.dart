import 'package:flutter/material.dart';

import '../widgets/column.dart';
import '../widgets/row.dart';
import '../widgets/cell.dart';

class Service {
  final List<MyCell> _listOfCells = [];

  List<dynamic> getListOfFields(List<dynamic> json) {
    List<dynamic> list = [];
    for (int i = 0; i < json.length; i++) {
      list.add(json[i]);
    }
    return list;
  }

  dynamic returnField(Map<String, dynamic> map) {
    String widgetName = map['widget'];
    switch (widgetName) {
      case 'column':
        return MyColumn(
          content: map['content'],
        );
      case 'row':
        return MyRow(
          content: map['content'],
        );
      case 'cell':
        final cell = MyCell(content: map['content']);
        _listOfCells.add(cell);
        return cell;
      default:
        return Container();
    }
  }

  List<MyCell> getListOfCells() {
    return _listOfCells;
  }
}

Service service = Service();
