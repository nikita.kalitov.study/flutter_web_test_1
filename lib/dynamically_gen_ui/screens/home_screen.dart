// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:flutter/material.dart';
import '../services/service.dart';
import '../json_sample.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<dynamic> list = service.getListOfFields(jsonDecode(json));
    // final List<>
    return Scaffold(
      body: Column(
        children: listForColumn(list)..add(elevatedButton),
      ),
    );
  }

  List<Widget> listForColumn(List<dynamic> list) {
    List<Widget> localList = [];
    for (int i = 0; i < list.length; i++) {
      localList.add(service.returnField(list[i]));
    }
    return localList;
  }

  final ElevatedButton elevatedButton = ElevatedButton(
    onPressed: () {
      final list = service.getListOfCells();
      for (int i = 0; i < list.length; i++) {
        // list[i].returnText();
        print(list[i].returnText());
      }
    },
    child: const Text('Check'),
  );
}
