// ignore_for_file: prefer_const_constructors_in_immutables, avoid_print

import 'package:flutter/material.dart';
import '../services/service.dart';

class MyRow extends StatelessWidget {
  const MyRow({
    Key? key,
    required this.content,
  }) : super(key: key);

  final List<dynamic> content;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: returnChild(),
    );
  }

  List<Widget> returnChild() {
    List<Widget> list = [];
    for (int i = 0; i < content.length; i++) {
      list.add(service.returnField(content[i]));
    }
    return list;
  }
}
