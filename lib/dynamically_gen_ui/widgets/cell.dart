// ignore_for_file: avoid_print, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import '../constants/sizes.dart';

class MyCell extends StatelessWidget {
  MyCell({
    Key? key,
    required this.content,
  }) : super(key: key);

  final textController = TextEditingController();

  final Map<String, dynamic> content;

  // @override
  // Widget build(BuildContext context) {
  //   return Container(
  //     child: const Text('I am cell!'),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: cellHeight.toDouble(),
      width: cellWidth.toDouble(),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.black),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
          controller: textController,
        ),
      ),
    );
  }

  String returnText() {
    return textController.text;
  }
}
