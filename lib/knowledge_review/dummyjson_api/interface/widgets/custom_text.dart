// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Text CustomText(String text, bool bold) {
  return Text(
    text,
    style: GoogleFonts.montserrat(
      fontSize: 25,
      fontWeight: bold ? FontWeight.bold : FontWeight.w400,
    ),
  );
}
