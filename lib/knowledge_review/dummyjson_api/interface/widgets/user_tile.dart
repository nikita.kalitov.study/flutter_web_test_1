// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../data/models/user.dart';
import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';

class UserTile extends StatelessWidget {
  const UserTile({Key? key, required this.user}) : super(key: key);

  final UserNullable user;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAppBloc, NewAppState>(
      builder: (context, state) {
        return ListTile(
          leading: Image.network(user.image!),
          title: Text('${user.firstName} ${user.lastName}'),
          subtitle: Text('${user.gender}, ${user.age}'),
          onTap: () {
            // print('i sent ${user.id}');
            context
                .read<NewAppBloc>()
                .add(NewAppEvent.goToSingleUserPage(currentUser: user));
          },
        );
      },
    );
  }
}
