// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';

class RootPage extends StatelessWidget {
  const RootPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAppBloc, NewAppState>(
      builder: (context, state) {
        context.read<NewAppBloc>().add(NewAppEvent.started());
        if (state.status == AppStatus.loaded) {
          context.read<NewAppBloc>().add(NewAppEvent.goToUsersPage());
        }
        return Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}
