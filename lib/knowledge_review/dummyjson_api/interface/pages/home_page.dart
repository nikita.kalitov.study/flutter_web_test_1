// ignore_for_file: prefer_const_constructors

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../data/api/api_client.dart';
import '../../data/api/dio_const.dart';
import '../../data/models/user.dart';
import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';
import '../../logic/cubit/home_page_cubit/home_page_cubit.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // print('build');
    // testFunction();
    // newFunction();
    return BlocBuilder<HomePageCubit, HomePageState>(
      builder: (context, state) {
        context.read<HomePageCubit>().createAndSaveLoginUser(UserLogin(
            username: 'kminchelle', password: '0lelplR', expiresInMins: 1));
        return Scaffold(
          body: Center(
            // child: CircularProgressIndicator(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  onPressed: () async {
                    await context
                        .read<HomePageCubit>()
                        .getNullableUserAndSave();
                  },
                  child: Text('Login'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    await context.read<HomePageCubit>().getAuthUser();
                  },
                  child: Text('GetAuthUser'),
                ),
                ElevatedButton(
                  onPressed: () async {
                    await context.read<HomePageCubit>().getAuthCarts();
                  },
                  child: Text('GetAuthCarts'),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  // void newFunction() async {
  //   BlocBuilder<HomePageCubit, HomePageState>(
  //     builder: (context, state) {
  //       return Container();
  //     },
  //   );
  // }
}

void testFunction() async {
  // final response = await ApiClient(dioConst).getUsers();
  // List<User> listOfUsers = response.users;
  // User user = listOfUsers[2];
  // print(user.firstName);
  // final response = await ApiClient(dioConst).getUserById(3);
  // print(response.firstName);
  // final response = await ApiClient(dioConst).getCartsByUserId(5);
  // List<Cart> listOfCarts = response.carts;
  // // print(listOfCarts);
  // listOfCarts.forEach((cart) {
  //   // print(cart.total);
  //   cart.products.forEach((product) {
  //     print(product.title);
  //   });
  // });

  final getUser = await ApiClient(dioConst).getUserById(1);
  // print('get user: $getUser');

  final UserNullable user = UserNullable(
    id: 1,
    // firstName: 'ABC',
    // lastName: 'abc',
    age: 25,
  );
  // print(user.toJson());
  final response =
      await ApiClient(dioConst).updateUser(2, user).catchError((object) {
    print((object as DioError).response?.data);
  });
  print(response);
}
