// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';
import '../../logic/cubit/users_page_cubit/users_page_cubit.dart';
import '../../routes.dart';
import '../widgets/user_tile.dart';

class UsersPage extends StatelessWidget {
  const UsersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UsersPageCubit, UsersPageState>(
      builder: (context, state) {
        context.read<UsersPageCubit>().getUsersList();
        return Scaffold(
          appBar: AppBar(title: Text(UsersPageName), centerTitle: true),
          body: state.isLoaded
              ? ListView.builder(
                  itemCount: state.listOfUser.length,
                  itemBuilder: (context, index) {
                    return UserTile(user: state.listOfUser[index]);
                  },
                )
              : Center(child: CircularProgressIndicator()),
        );
      },
    );
  }
}
