// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';
import '../../logic/cubit/single_user_page_cubit/single_user_page_cubit.dart';
import '../../routes.dart';
import '../widgets/single_user_info.dart';

class SingleUserPage extends StatelessWidget {
  const SingleUserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAppBloc, NewAppState>(
      builder: (context, newAppState) {
        return Scaffold(
          appBar: AppBar(
            title: Text(SingleUserPageName),
            centerTitle: true,
            actions: [
              IconButton(
                onPressed: () {
                  context
                      .read<NewAppBloc>()
                      .add(NewAppEvent.goToUserCartPage());
                },
                icon: Icon(Icons.shopping_cart),
              ),
            ],
          ),
          body: BlocBuilder<SingleUserPageCubit, SingleUserPageState>(
            builder: (context, cubitState) {
              // if (newAppState.currentUser?.id == -1) {
              //   // context.read<NewAppBloc>().add(NewAppEvent.goToErrorPage());
              // }
              context
                  .read<SingleUserPageCubit>()
                  // .getSingleUserById(2);
                  .getSingleUserById(newAppState.currentUser!.id!);
              return (cubitState.isLoaded)
                  ? SingleUserInfo(user: cubitState.user)
                  : Center(
                      child: CircularProgressIndicator(),
                    );
            },
          ),
        );
      },
    );
  }
}
