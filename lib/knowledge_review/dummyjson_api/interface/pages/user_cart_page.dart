// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../logic/bloc/new_app_bloc/new_app_bloc.dart';
import '../../logic/cubit/user_cart_page_cubit/user_cart_page_cubit.dart';
import '../widgets/custom_text.dart';
import '../../routes.dart';

class UserCartPage extends StatelessWidget {
  const UserCartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewAppBloc, NewAppState>(
      builder: (context, appState) {
        return BlocBuilder<UserCartPageCubit, UserCartPageState>(
          builder: (context, cubitState) {
            context
                .read<UserCartPageCubit>()
                .getUserCartById(appState.currentUser!.id!);
            return Scaffold(
              appBar: AppBar(
                title: Text(UserCartPageName),
                centerTitle: true,
              ),
              body: cubitState.isLoaded
                  ? ListView.builder(
                      itemCount: cubitState.listOfCarts.length,
                      itemBuilder: (context, index) {
                        print(cubitState.listOfCarts.length);
                        return Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: cubitState.listOfCarts.length == 0
                              ? Center(
                                  child: CustomText('Cart is empty!', true),
                                )
                              : SingleChildScrollView(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          CustomText(
                                            'Cart id: ',
                                            true,
                                          ),
                                          CustomText(
                                            '${cubitState.listOfCarts[index].id}',
                                            false,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          CustomText(
                                            'User id: ',
                                            true,
                                          ),
                                          CustomText(
                                            '${cubitState.listOfCarts[index].userId}',
                                            false,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: [
                                          CustomText(
                                            'Total: ',
                                            true,
                                          ),
                                          CustomText(
                                            '${cubitState.listOfCarts[index].total}',
                                            false,
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          ElevatedButton(
                                            onPressed: () {},
                                            child: Text('Show products'),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                        );
                      },
                    )
                  : Center(child: CircularProgressIndicator()),
            );
          },
        );
      },
    );
  }
}
