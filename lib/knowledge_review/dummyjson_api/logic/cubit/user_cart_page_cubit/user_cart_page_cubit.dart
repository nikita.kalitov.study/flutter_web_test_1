// ignore_for_file: prefer_const_constructors

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../data/models/cart.dart';
import '../../../data/api/api_client.dart';
import '../../../data/api/responses/get_cart_by_id.dart';
import '../../../data/api/dio_const.dart';

part 'user_cart_page_state.dart';
part 'user_cart_page_cubit.freezed.dart';

class UserCartPageCubit extends Cubit<UserCartPageState> {
  UserCartPageCubit()
      : super(UserCartPageState.initial(
          isLoaded: false,
          listOfCarts: [],
        ));

  void getUserCartById(int id) async {
    final GetCartsByUserId response =
        await ApiClient(dioConst).getCartsByUserId(id);
    emit(state.copyWith(isLoaded: true, listOfCarts: response.carts));
  }
}
