// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_cart_page_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserCartPageState {
  bool get isLoaded => throw _privateConstructorUsedError;
  List<Cart> get listOfCarts => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isLoaded, List<Cart> listOfCarts) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isLoaded, List<Cart> listOfCarts)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isLoaded, List<Cart> listOfCarts)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserCartPageStateCopyWith<UserCartPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCartPageStateCopyWith<$Res> {
  factory $UserCartPageStateCopyWith(
          UserCartPageState value, $Res Function(UserCartPageState) then) =
      _$UserCartPageStateCopyWithImpl<$Res>;
  $Res call({bool isLoaded, List<Cart> listOfCarts});
}

/// @nodoc
class _$UserCartPageStateCopyWithImpl<$Res>
    implements $UserCartPageStateCopyWith<$Res> {
  _$UserCartPageStateCopyWithImpl(this._value, this._then);

  final UserCartPageState _value;
  // ignore: unused_field
  final $Res Function(UserCartPageState) _then;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? listOfCarts = freezed,
  }) {
    return _then(_value.copyWith(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool,
      listOfCarts: listOfCarts == freezed
          ? _value.listOfCarts
          : listOfCarts // ignore: cast_nullable_to_non_nullable
              as List<Cart>,
    ));
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res>
    implements $UserCartPageStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  $Res call({bool isLoaded, List<Cart> listOfCarts});
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$UserCartPageStateCopyWithImpl<$Res>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, (v) => _then(v as _$_Initial));

  @override
  _$_Initial get _value => super._value as _$_Initial;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? listOfCarts = freezed,
  }) {
    return _then(_$_Initial(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool,
      listOfCarts: listOfCarts == freezed
          ? _value._listOfCarts
          : listOfCarts // ignore: cast_nullable_to_non_nullable
              as List<Cart>,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(
      {required this.isLoaded, required final List<Cart> listOfCarts})
      : _listOfCarts = listOfCarts;

  @override
  final bool isLoaded;
  final List<Cart> _listOfCarts;
  @override
  List<Cart> get listOfCarts {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_listOfCarts);
  }

  @override
  String toString() {
    return 'UserCartPageState.initial(isLoaded: $isLoaded, listOfCarts: $listOfCarts)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            const DeepCollectionEquality().equals(other.isLoaded, isLoaded) &&
            const DeepCollectionEquality()
                .equals(other._listOfCarts, _listOfCarts));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isLoaded),
      const DeepCollectionEquality().hash(_listOfCarts));

  @JsonKey(ignore: true)
  @override
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isLoaded, List<Cart> listOfCarts) initial,
  }) {
    return initial(isLoaded, listOfCarts);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isLoaded, List<Cart> listOfCarts)? initial,
  }) {
    return initial?.call(isLoaded, listOfCarts);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isLoaded, List<Cart> listOfCarts)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(isLoaded, listOfCarts);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements UserCartPageState {
  const factory _Initial(
      {required final bool isLoaded,
      required final List<Cart> listOfCarts}) = _$_Initial;

  @override
  bool get isLoaded;
  @override
  List<Cart> get listOfCarts;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
