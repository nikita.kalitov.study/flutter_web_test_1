part of 'user_cart_page_cubit.dart';

@freezed
class UserCartPageState with _$UserCartPageState {
  const factory UserCartPageState.initial({
    required bool isLoaded,
    required List<Cart> listOfCarts,
  }) = _Initial;
}
