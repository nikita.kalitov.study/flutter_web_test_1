// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'home_page_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomePageState {
  String? get accessToken => throw _privateConstructorUsedError;
  UserNullable? get userNullable => throw _privateConstructorUsedError;
  UserLogin? get userLogin => throw _privateConstructorUsedError;
  Dio? get dio => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)?
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)?
        initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $HomePageStateCopyWith<HomePageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomePageStateCopyWith<$Res> {
  factory $HomePageStateCopyWith(
          HomePageState value, $Res Function(HomePageState) then) =
      _$HomePageStateCopyWithImpl<$Res>;
  $Res call(
      {String? accessToken,
      UserNullable? userNullable,
      UserLogin? userLogin,
      Dio? dio});

  $UserNullableCopyWith<$Res>? get userNullable;
  $UserLoginCopyWith<$Res>? get userLogin;
}

/// @nodoc
class _$HomePageStateCopyWithImpl<$Res>
    implements $HomePageStateCopyWith<$Res> {
  _$HomePageStateCopyWithImpl(this._value, this._then);

  final HomePageState _value;
  // ignore: unused_field
  final $Res Function(HomePageState) _then;

  @override
  $Res call({
    Object? accessToken = freezed,
    Object? userNullable = freezed,
    Object? userLogin = freezed,
    Object? dio = freezed,
  }) {
    return _then(_value.copyWith(
      accessToken: accessToken == freezed
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
      userNullable: userNullable == freezed
          ? _value.userNullable
          : userNullable // ignore: cast_nullable_to_non_nullable
              as UserNullable?,
      userLogin: userLogin == freezed
          ? _value.userLogin
          : userLogin // ignore: cast_nullable_to_non_nullable
              as UserLogin?,
      dio: dio == freezed
          ? _value.dio
          : dio // ignore: cast_nullable_to_non_nullable
              as Dio?,
    ));
  }

  @override
  $UserNullableCopyWith<$Res>? get userNullable {
    if (_value.userNullable == null) {
      return null;
    }

    return $UserNullableCopyWith<$Res>(_value.userNullable!, (value) {
      return _then(_value.copyWith(userNullable: value));
    });
  }

  @override
  $UserLoginCopyWith<$Res>? get userLogin {
    if (_value.userLogin == null) {
      return null;
    }

    return $UserLoginCopyWith<$Res>(_value.userLogin!, (value) {
      return _then(_value.copyWith(userLogin: value));
    });
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res>
    implements $HomePageStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  $Res call(
      {String? accessToken,
      UserNullable? userNullable,
      UserLogin? userLogin,
      Dio? dio});

  @override
  $UserNullableCopyWith<$Res>? get userNullable;
  @override
  $UserLoginCopyWith<$Res>? get userLogin;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res> extends _$HomePageStateCopyWithImpl<$Res>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, (v) => _then(v as _$_Initial));

  @override
  _$_Initial get _value => super._value as _$_Initial;

  @override
  $Res call({
    Object? accessToken = freezed,
    Object? userNullable = freezed,
    Object? userLogin = freezed,
    Object? dio = freezed,
  }) {
    return _then(_$_Initial(
      accessToken: accessToken == freezed
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
      userNullable: userNullable == freezed
          ? _value.userNullable
          : userNullable // ignore: cast_nullable_to_non_nullable
              as UserNullable?,
      userLogin: userLogin == freezed
          ? _value.userLogin
          : userLogin // ignore: cast_nullable_to_non_nullable
              as UserLogin?,
      dio: dio == freezed
          ? _value.dio
          : dio // ignore: cast_nullable_to_non_nullable
              as Dio?,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  _$_Initial({this.accessToken, this.userNullable, this.userLogin, this.dio});

  @override
  final String? accessToken;
  @override
  final UserNullable? userNullable;
  @override
  final UserLogin? userLogin;
  @override
  final Dio? dio;

  @override
  String toString() {
    return 'HomePageState.initial(accessToken: $accessToken, userNullable: $userNullable, userLogin: $userLogin, dio: $dio)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            const DeepCollectionEquality()
                .equals(other.accessToken, accessToken) &&
            const DeepCollectionEquality()
                .equals(other.userNullable, userNullable) &&
            const DeepCollectionEquality().equals(other.userLogin, userLogin) &&
            const DeepCollectionEquality().equals(other.dio, dio));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(accessToken),
      const DeepCollectionEquality().hash(userNullable),
      const DeepCollectionEquality().hash(userLogin),
      const DeepCollectionEquality().hash(dio));

  @JsonKey(ignore: true)
  @override
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)
        initial,
  }) {
    return initial(accessToken, userNullable, userLogin, dio);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)?
        initial,
  }) {
    return initial?.call(accessToken, userNullable, userLogin, dio);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String? accessToken, UserNullable? userNullable,
            UserLogin? userLogin, Dio? dio)?
        initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(accessToken, userNullable, userLogin, dio);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements HomePageState {
  factory _Initial(
      {final String? accessToken,
      final UserNullable? userNullable,
      final UserLogin? userLogin,
      final Dio? dio}) = _$_Initial;

  @override
  String? get accessToken;
  @override
  UserNullable? get userNullable;
  @override
  UserLogin? get userLogin;
  @override
  Dio? get dio;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
