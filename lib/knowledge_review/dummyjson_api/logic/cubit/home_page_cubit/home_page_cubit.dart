// ignore_for_file: prefer_const_constructors

import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../data/api/api_client.dart';
import '../../../data/models/user.dart';
import '../../../data/api/dio_const.dart';

part 'home_page_state.dart';
part 'home_page_cubit.freezed.dart';

class HomePageCubit extends Cubit<HomePageState> {
  HomePageCubit()
      : super(HomePageState.initial(
            dio: Dio(BaseOptions(headers: {
          // 'Authorization': 'Bearer ${state.accessToken}',
          'Content-Type': 'application/json',
        }))));

  Future<void> createAndSaveLoginUser(UserLogin user) async {
    emit(state.copyWith(userLogin: user));
  }

  Future<void> getNullableUserAndSave() async {
    final UserNullable response =
        await ApiClient(dioConst).login(state.userLogin!);
    emit(state.copyWith(accessToken: response.token, userNullable: response));
  }

  Future<void> getAuthUser() async {
    Dio dio = Dio(BaseOptions(headers: {
      'Authorization': 'Bearer ${state.accessToken}',
      'Content-Type': 'application/json',
    }));
    // dio.interceptors.add(element);
    final response =
        await ApiClient(dio).getAuthUserById(state.userNullable!.id!);
    print(response.email);
  }

  Future<void> getAuthCarts() async {
    Dio dio = Dio(BaseOptions(headers: {
      'Authorization': 'Bearer ${state.accessToken}',
      'Content-Type': 'application/json',
    }));
    final response =
        await ApiClient(dio).getAuthCartsByUserId(state.userNullable!.id!);
    print(response.carts.length);
  }
}
