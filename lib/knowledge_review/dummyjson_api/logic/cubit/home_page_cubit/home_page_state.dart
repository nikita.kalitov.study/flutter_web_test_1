part of 'home_page_cubit.dart';

@freezed
class HomePageState with _$HomePageState {
  factory HomePageState.initial({
    String? accessToken,
    UserNullable? userNullable,
    UserLogin? userLogin,
    Dio? dio,
  }) = _Initial;
}
