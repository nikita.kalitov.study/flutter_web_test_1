// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'users_page_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UsersPageState {
  bool get isLoaded => throw _privateConstructorUsedError;
  List<UserNullable> get listOfUser => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isLoaded, List<UserNullable> listOfUser)
        initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isLoaded, List<UserNullable> listOfUser)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isLoaded, List<UserNullable> listOfUser)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UsersPageStateCopyWith<UsersPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UsersPageStateCopyWith<$Res> {
  factory $UsersPageStateCopyWith(
          UsersPageState value, $Res Function(UsersPageState) then) =
      _$UsersPageStateCopyWithImpl<$Res>;
  $Res call({bool isLoaded, List<UserNullable> listOfUser});
}

/// @nodoc
class _$UsersPageStateCopyWithImpl<$Res>
    implements $UsersPageStateCopyWith<$Res> {
  _$UsersPageStateCopyWithImpl(this._value, this._then);

  final UsersPageState _value;
  // ignore: unused_field
  final $Res Function(UsersPageState) _then;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? listOfUser = freezed,
  }) {
    return _then(_value.copyWith(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool,
      listOfUser: listOfUser == freezed
          ? _value.listOfUser
          : listOfUser // ignore: cast_nullable_to_non_nullable
              as List<UserNullable>,
    ));
  }
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res>
    implements $UsersPageStateCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
  @override
  $Res call({bool isLoaded, List<UserNullable> listOfUser});
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res> extends _$UsersPageStateCopyWithImpl<$Res>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, (v) => _then(v as _$_Initial));

  @override
  _$_Initial get _value => super._value as _$_Initial;

  @override
  $Res call({
    Object? isLoaded = freezed,
    Object? listOfUser = freezed,
  }) {
    return _then(_$_Initial(
      isLoaded: isLoaded == freezed
          ? _value.isLoaded
          : isLoaded // ignore: cast_nullable_to_non_nullable
              as bool,
      listOfUser: listOfUser == freezed
          ? _value._listOfUser
          : listOfUser // ignore: cast_nullable_to_non_nullable
              as List<UserNullable>,
    ));
  }
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial(
      {required this.isLoaded, required final List<UserNullable> listOfUser})
      : _listOfUser = listOfUser;

  @override
  final bool isLoaded;
  final List<UserNullable> _listOfUser;
  @override
  List<UserNullable> get listOfUser {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_listOfUser);
  }

  @override
  String toString() {
    return 'UsersPageState.initial(isLoaded: $isLoaded, listOfUser: $listOfUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Initial &&
            const DeepCollectionEquality().equals(other.isLoaded, isLoaded) &&
            const DeepCollectionEquality()
                .equals(other._listOfUser, _listOfUser));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isLoaded),
      const DeepCollectionEquality().hash(_listOfUser));

  @JsonKey(ignore: true)
  @override
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      __$$_InitialCopyWithImpl<_$_Initial>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(bool isLoaded, List<UserNullable> listOfUser)
        initial,
  }) {
    return initial(isLoaded, listOfUser);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(bool isLoaded, List<UserNullable> listOfUser)? initial,
  }) {
    return initial?.call(isLoaded, listOfUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(bool isLoaded, List<UserNullable> listOfUser)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(isLoaded, listOfUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements UsersPageState {
  const factory _Initial(
      {required final bool isLoaded,
      required final List<UserNullable> listOfUser}) = _$_Initial;

  @override
  bool get isLoaded;
  @override
  List<UserNullable> get listOfUser;
  @override
  @JsonKey(ignore: true)
  _$$_InitialCopyWith<_$_Initial> get copyWith =>
      throw _privateConstructorUsedError;
}
