part of 'users_page_cubit.dart';

@freezed
class UsersPageState with _$UsersPageState {
  const factory UsersPageState.initial({
    required bool isLoaded,
    required List<UserNullable> listOfUser,
  }) = _Initial;
}
