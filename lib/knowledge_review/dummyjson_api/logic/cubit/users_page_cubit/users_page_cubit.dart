// ignore_for_file: prefer_const_constructors

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../data/models/models.dart';
import '../../../data/api/api_client.dart';
import '../../../data/api/dio_const.dart';
import '../../../data/api/responses/get_users.dart';
import '../single_user_page_cubit/single_user_page_cubit.dart';

part 'users_page_state.dart';
part 'users_page_cubit.freezed.dart';

class UsersPageCubit extends Cubit<UsersPageState> {
  UsersPageCubit()
      : super(UsersPageState.initial(isLoaded: false, listOfUser: []));

  void getUsersList() async {
    final GetUsersResponse response = await ApiClient(dioConst).getUsers();
    final List<UserNullable> listOfUsers = response.users;
    emit(state.copyWith(isLoaded: true, listOfUser: listOfUsers));
  }
}
