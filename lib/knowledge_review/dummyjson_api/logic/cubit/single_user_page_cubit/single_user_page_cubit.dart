// ignore_for_file: prefer_const_constructors

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../data/models/user.dart';
import '../../../data/api/api_client.dart';
import '../../../data/api/dio_const.dart';

part 'single_user_page_state.dart';
part 'single_user_page_cubit.freezed.dart';

class SingleUserPageCubit extends Cubit<SingleUserPageState> {
  SingleUserPageCubit()
      : super(SingleUserPageState.initial(
          isLoaded: false,
          user: UserNullable(),
        ));

  void getSingleUserById(int id) async {
    final UserNullable user = await ApiClient(dioConst).getUserById(id);
    emit(state.copyWith(isLoaded: true, user: user));
  }
}
