part of 'single_user_page_cubit.dart';

@freezed
class SingleUserPageState with _$SingleUserPageState {
  const factory SingleUserPageState.initial(
      {required bool isLoaded, required UserNullable user}) = _Initial;
}
