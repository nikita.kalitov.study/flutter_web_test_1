// ignore_for_file: prefer_const_constructors

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import '../../../router.dart';
import '../../../routes.dart';
import '../../../data/models/models.dart';

part 'new_app_event.dart';
part 'new_app_state.dart';
part 'new_app_bloc.freezed.dart';

class NewAppBloc extends Bloc<NewAppEvent, NewAppState> {
  NewAppBloc() : super(_AppState(status: AppStatus.loading)) {
    on<_Loading>(_onLoading);
    on<_PageChanged>(_onPageChanged);
    on<_GoToRootPage>(_onGoToRootPage);
    on<_GoToUsersPage>(_onGoToUsersPage);
    on<_GoToSingleUserPage>(_onGoToSingleUserPage);
    on<_GoToUserCartPage>(_onGoToUserCartPage);
    on<_GoToErrorPage>(_onGoToErrorPage);
    on<_GoToHomePage>(_onGoToHomePage);
  }

  Future<void> _onLoading(_Loading event, Emitter<NewAppState> emit) async {
    await Future.delayed(Duration(seconds: 1));
    emit(state.copyWith(status: AppStatus.loaded));
  }

  Future<void> _onPageChanged(
      _PageChanged event, Emitter<NewAppState> emit) async {
    router.go(event.route);
  }

  Future<void> _onGoToRootPage(
      _GoToRootPage event, Emitter<NewAppState> emit) async {
    router.go(RootPageRoute);
  }

  Future<void> _onGoToUsersPage(
      _GoToUsersPage event, Emitter<NewAppState> emit) async {
    router.go(UsersPageRoute);
  }

  Future<void> _onGoToSingleUserPage(
      _GoToSingleUserPage event, Emitter<NewAppState> emit) async {
    emit(state.copyWith(currentUser: event.currentUser));
    router.go(SingleUserPageRoute);
  }

  Future<void> _onGoToUserCartPage(
      _GoToUserCartPage event, Emitter<NewAppState> emit) async {
    router.go(UserCartPageRoute);
  }

  Future<void> _onGoToErrorPage(
      _GoToErrorPage event, Emitter<NewAppState> emit) async {
    router.go(ErrorPageRoute);
  }

  Future<void> _onGoToHomePage(
      _GoToHomePage event, Emitter<NewAppState> emit) async {
    router.go(HomePageRoute);
  }
}
