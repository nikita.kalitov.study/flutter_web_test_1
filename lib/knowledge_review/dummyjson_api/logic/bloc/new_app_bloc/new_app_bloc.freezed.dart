// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'new_app_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$NewAppEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NewAppEventCopyWith<$Res> {
  factory $NewAppEventCopyWith(
          NewAppEvent value, $Res Function(NewAppEvent) then) =
      _$NewAppEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$NewAppEventCopyWithImpl<$Res> implements $NewAppEventCopyWith<$Res> {
  _$NewAppEventCopyWithImpl(this._value, this._then);

  final NewAppEvent _value;
  // ignore: unused_field
  final $Res Function(NewAppEvent) _then;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res> extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, (v) => _then(v as _$_Loading));

  @override
  _$_Loading get _value => super._value as _$_Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'NewAppEvent.started()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Loading implements NewAppEvent {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_PageChangedCopyWith<$Res> {
  factory _$$_PageChangedCopyWith(
          _$_PageChanged value, $Res Function(_$_PageChanged) then) =
      __$$_PageChangedCopyWithImpl<$Res>;
  $Res call({String route});
}

/// @nodoc
class __$$_PageChangedCopyWithImpl<$Res> extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_PageChangedCopyWith<$Res> {
  __$$_PageChangedCopyWithImpl(
      _$_PageChanged _value, $Res Function(_$_PageChanged) _then)
      : super(_value, (v) => _then(v as _$_PageChanged));

  @override
  _$_PageChanged get _value => super._value as _$_PageChanged;

  @override
  $Res call({
    Object? route = freezed,
  }) {
    return _then(_$_PageChanged(
      route: route == freezed
          ? _value.route
          : route // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_PageChanged implements _PageChanged {
  const _$_PageChanged({required this.route});

  @override
  final String route;

  @override
  String toString() {
    return 'NewAppEvent.pageChanged(route: $route)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PageChanged &&
            const DeepCollectionEquality().equals(other.route, route));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(route));

  @JsonKey(ignore: true)
  @override
  _$$_PageChangedCopyWith<_$_PageChanged> get copyWith =>
      __$$_PageChangedCopyWithImpl<_$_PageChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return pageChanged(route);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return pageChanged?.call(route);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (pageChanged != null) {
      return pageChanged(route);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return pageChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return pageChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (pageChanged != null) {
      return pageChanged(this);
    }
    return orElse();
  }
}

abstract class _PageChanged implements NewAppEvent {
  const factory _PageChanged({required final String route}) = _$_PageChanged;

  String get route;
  @JsonKey(ignore: true)
  _$$_PageChangedCopyWith<_$_PageChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GoToRootPageCopyWith<$Res> {
  factory _$$_GoToRootPageCopyWith(
          _$_GoToRootPage value, $Res Function(_$_GoToRootPage) then) =
      __$$_GoToRootPageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GoToRootPageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToRootPageCopyWith<$Res> {
  __$$_GoToRootPageCopyWithImpl(
      _$_GoToRootPage _value, $Res Function(_$_GoToRootPage) _then)
      : super(_value, (v) => _then(v as _$_GoToRootPage));

  @override
  _$_GoToRootPage get _value => super._value as _$_GoToRootPage;
}

/// @nodoc

class _$_GoToRootPage implements _GoToRootPage {
  const _$_GoToRootPage();

  @override
  String toString() {
    return 'NewAppEvent.goToRootPage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GoToRootPage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToRootPage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToRootPage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToRootPage != null) {
      return goToRootPage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToRootPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToRootPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToRootPage != null) {
      return goToRootPage(this);
    }
    return orElse();
  }
}

abstract class _GoToRootPage implements NewAppEvent {
  const factory _GoToRootPage() = _$_GoToRootPage;
}

/// @nodoc
abstract class _$$_GoToUsersPageCopyWith<$Res> {
  factory _$$_GoToUsersPageCopyWith(
          _$_GoToUsersPage value, $Res Function(_$_GoToUsersPage) then) =
      __$$_GoToUsersPageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GoToUsersPageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToUsersPageCopyWith<$Res> {
  __$$_GoToUsersPageCopyWithImpl(
      _$_GoToUsersPage _value, $Res Function(_$_GoToUsersPage) _then)
      : super(_value, (v) => _then(v as _$_GoToUsersPage));

  @override
  _$_GoToUsersPage get _value => super._value as _$_GoToUsersPage;
}

/// @nodoc

class _$_GoToUsersPage implements _GoToUsersPage {
  const _$_GoToUsersPage();

  @override
  String toString() {
    return 'NewAppEvent.goToUsersPage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GoToUsersPage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToUsersPage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToUsersPage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToUsersPage != null) {
      return goToUsersPage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToUsersPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToUsersPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToUsersPage != null) {
      return goToUsersPage(this);
    }
    return orElse();
  }
}

abstract class _GoToUsersPage implements NewAppEvent {
  const factory _GoToUsersPage() = _$_GoToUsersPage;
}

/// @nodoc
abstract class _$$_GoToSingleUserPageCopyWith<$Res> {
  factory _$$_GoToSingleUserPageCopyWith(_$_GoToSingleUserPage value,
          $Res Function(_$_GoToSingleUserPage) then) =
      __$$_GoToSingleUserPageCopyWithImpl<$Res>;
  $Res call({UserNullable currentUser});

  $UserNullableCopyWith<$Res> get currentUser;
}

/// @nodoc
class __$$_GoToSingleUserPageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToSingleUserPageCopyWith<$Res> {
  __$$_GoToSingleUserPageCopyWithImpl(
      _$_GoToSingleUserPage _value, $Res Function(_$_GoToSingleUserPage) _then)
      : super(_value, (v) => _then(v as _$_GoToSingleUserPage));

  @override
  _$_GoToSingleUserPage get _value => super._value as _$_GoToSingleUserPage;

  @override
  $Res call({
    Object? currentUser = freezed,
  }) {
    return _then(_$_GoToSingleUserPage(
      currentUser: currentUser == freezed
          ? _value.currentUser
          : currentUser // ignore: cast_nullable_to_non_nullable
              as UserNullable,
    ));
  }

  @override
  $UserNullableCopyWith<$Res> get currentUser {
    return $UserNullableCopyWith<$Res>(_value.currentUser, (value) {
      return _then(_value.copyWith(currentUser: value));
    });
  }
}

/// @nodoc

class _$_GoToSingleUserPage implements _GoToSingleUserPage {
  const _$_GoToSingleUserPage({required this.currentUser});

  @override
  final UserNullable currentUser;

  @override
  String toString() {
    return 'NewAppEvent.goToSingleUserPage(currentUser: $currentUser)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_GoToSingleUserPage &&
            const DeepCollectionEquality()
                .equals(other.currentUser, currentUser));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(currentUser));

  @JsonKey(ignore: true)
  @override
  _$$_GoToSingleUserPageCopyWith<_$_GoToSingleUserPage> get copyWith =>
      __$$_GoToSingleUserPageCopyWithImpl<_$_GoToSingleUserPage>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToSingleUserPage(currentUser);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToSingleUserPage?.call(currentUser);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToSingleUserPage != null) {
      return goToSingleUserPage(currentUser);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToSingleUserPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToSingleUserPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToSingleUserPage != null) {
      return goToSingleUserPage(this);
    }
    return orElse();
  }
}

abstract class _GoToSingleUserPage implements NewAppEvent {
  const factory _GoToSingleUserPage({required final UserNullable currentUser}) =
      _$_GoToSingleUserPage;

  UserNullable get currentUser;
  @JsonKey(ignore: true)
  _$$_GoToSingleUserPageCopyWith<_$_GoToSingleUserPage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_GoToUserCartPageCopyWith<$Res> {
  factory _$$_GoToUserCartPageCopyWith(
          _$_GoToUserCartPage value, $Res Function(_$_GoToUserCartPage) then) =
      __$$_GoToUserCartPageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GoToUserCartPageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToUserCartPageCopyWith<$Res> {
  __$$_GoToUserCartPageCopyWithImpl(
      _$_GoToUserCartPage _value, $Res Function(_$_GoToUserCartPage) _then)
      : super(_value, (v) => _then(v as _$_GoToUserCartPage));

  @override
  _$_GoToUserCartPage get _value => super._value as _$_GoToUserCartPage;
}

/// @nodoc

class _$_GoToUserCartPage implements _GoToUserCartPage {
  const _$_GoToUserCartPage();

  @override
  String toString() {
    return 'NewAppEvent.goToUserCartPage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GoToUserCartPage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToUserCartPage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToUserCartPage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToUserCartPage != null) {
      return goToUserCartPage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToUserCartPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToUserCartPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToUserCartPage != null) {
      return goToUserCartPage(this);
    }
    return orElse();
  }
}

abstract class _GoToUserCartPage implements NewAppEvent {
  const factory _GoToUserCartPage() = _$_GoToUserCartPage;
}

/// @nodoc
abstract class _$$_GoToErrorPageCopyWith<$Res> {
  factory _$$_GoToErrorPageCopyWith(
          _$_GoToErrorPage value, $Res Function(_$_GoToErrorPage) then) =
      __$$_GoToErrorPageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GoToErrorPageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToErrorPageCopyWith<$Res> {
  __$$_GoToErrorPageCopyWithImpl(
      _$_GoToErrorPage _value, $Res Function(_$_GoToErrorPage) _then)
      : super(_value, (v) => _then(v as _$_GoToErrorPage));

  @override
  _$_GoToErrorPage get _value => super._value as _$_GoToErrorPage;
}

/// @nodoc

class _$_GoToErrorPage implements _GoToErrorPage {
  const _$_GoToErrorPage();

  @override
  String toString() {
    return 'NewAppEvent.goToErrorPage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GoToErrorPage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToErrorPage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToErrorPage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToErrorPage != null) {
      return goToErrorPage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToErrorPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToErrorPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToErrorPage != null) {
      return goToErrorPage(this);
    }
    return orElse();
  }
}

abstract class _GoToErrorPage implements NewAppEvent {
  const factory _GoToErrorPage() = _$_GoToErrorPage;
}

/// @nodoc
abstract class _$$_GoToHomePageCopyWith<$Res> {
  factory _$$_GoToHomePageCopyWith(
          _$_GoToHomePage value, $Res Function(_$_GoToHomePage) then) =
      __$$_GoToHomePageCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_GoToHomePageCopyWithImpl<$Res>
    extends _$NewAppEventCopyWithImpl<$Res>
    implements _$$_GoToHomePageCopyWith<$Res> {
  __$$_GoToHomePageCopyWithImpl(
      _$_GoToHomePage _value, $Res Function(_$_GoToHomePage) _then)
      : super(_value, (v) => _then(v as _$_GoToHomePage));

  @override
  _$_GoToHomePage get _value => super._value as _$_GoToHomePage;
}

/// @nodoc

class _$_GoToHomePage implements _GoToHomePage {
  const _$_GoToHomePage();

  @override
  String toString() {
    return 'NewAppEvent.goToHomePage()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_GoToHomePage);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(String route) pageChanged,
    required TResult Function() goToRootPage,
    required TResult Function() goToUsersPage,
    required TResult Function(UserNullable currentUser) goToSingleUserPage,
    required TResult Function() goToUserCartPage,
    required TResult Function() goToErrorPage,
    required TResult Function() goToHomePage,
  }) {
    return goToHomePage();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
  }) {
    return goToHomePage?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(String route)? pageChanged,
    TResult Function()? goToRootPage,
    TResult Function()? goToUsersPage,
    TResult Function(UserNullable currentUser)? goToSingleUserPage,
    TResult Function()? goToUserCartPage,
    TResult Function()? goToErrorPage,
    TResult Function()? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToHomePage != null) {
      return goToHomePage();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) started,
    required TResult Function(_PageChanged value) pageChanged,
    required TResult Function(_GoToRootPage value) goToRootPage,
    required TResult Function(_GoToUsersPage value) goToUsersPage,
    required TResult Function(_GoToSingleUserPage value) goToSingleUserPage,
    required TResult Function(_GoToUserCartPage value) goToUserCartPage,
    required TResult Function(_GoToErrorPage value) goToErrorPage,
    required TResult Function(_GoToHomePage value) goToHomePage,
  }) {
    return goToHomePage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
  }) {
    return goToHomePage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? started,
    TResult Function(_PageChanged value)? pageChanged,
    TResult Function(_GoToRootPage value)? goToRootPage,
    TResult Function(_GoToUsersPage value)? goToUsersPage,
    TResult Function(_GoToSingleUserPage value)? goToSingleUserPage,
    TResult Function(_GoToUserCartPage value)? goToUserCartPage,
    TResult Function(_GoToErrorPage value)? goToErrorPage,
    TResult Function(_GoToHomePage value)? goToHomePage,
    required TResult orElse(),
  }) {
    if (goToHomePage != null) {
      return goToHomePage(this);
    }
    return orElse();
  }
}

abstract class _GoToHomePage implements NewAppEvent {
  const factory _GoToHomePage() = _$_GoToHomePage;
}

/// @nodoc
mixin _$NewAppState {
  AppStatus get status => throw _privateConstructorUsedError;
  UserNullable? get currentUser => throw _privateConstructorUsedError;
  Cart? get currentCart => throw _privateConstructorUsedError;
  String? get accessToken => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $NewAppStateCopyWith<NewAppState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NewAppStateCopyWith<$Res> {
  factory $NewAppStateCopyWith(
          NewAppState value, $Res Function(NewAppState) then) =
      _$NewAppStateCopyWithImpl<$Res>;
  $Res call(
      {AppStatus status,
      UserNullable? currentUser,
      Cart? currentCart,
      String? accessToken});

  $UserNullableCopyWith<$Res>? get currentUser;
  $CartCopyWith<$Res>? get currentCart;
}

/// @nodoc
class _$NewAppStateCopyWithImpl<$Res> implements $NewAppStateCopyWith<$Res> {
  _$NewAppStateCopyWithImpl(this._value, this._then);

  final NewAppState _value;
  // ignore: unused_field
  final $Res Function(NewAppState) _then;

  @override
  $Res call({
    Object? status = freezed,
    Object? currentUser = freezed,
    Object? currentCart = freezed,
    Object? accessToken = freezed,
  }) {
    return _then(_value.copyWith(
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as AppStatus,
      currentUser: currentUser == freezed
          ? _value.currentUser
          : currentUser // ignore: cast_nullable_to_non_nullable
              as UserNullable?,
      currentCart: currentCart == freezed
          ? _value.currentCart
          : currentCart // ignore: cast_nullable_to_non_nullable
              as Cart?,
      accessToken: accessToken == freezed
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }

  @override
  $UserNullableCopyWith<$Res>? get currentUser {
    if (_value.currentUser == null) {
      return null;
    }

    return $UserNullableCopyWith<$Res>(_value.currentUser!, (value) {
      return _then(_value.copyWith(currentUser: value));
    });
  }

  @override
  $CartCopyWith<$Res>? get currentCart {
    if (_value.currentCart == null) {
      return null;
    }

    return $CartCopyWith<$Res>(_value.currentCart!, (value) {
      return _then(_value.copyWith(currentCart: value));
    });
  }
}

/// @nodoc
abstract class _$$_AppStateCopyWith<$Res>
    implements $NewAppStateCopyWith<$Res> {
  factory _$$_AppStateCopyWith(
          _$_AppState value, $Res Function(_$_AppState) then) =
      __$$_AppStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {AppStatus status,
      UserNullable? currentUser,
      Cart? currentCart,
      String? accessToken});

  @override
  $UserNullableCopyWith<$Res>? get currentUser;
  @override
  $CartCopyWith<$Res>? get currentCart;
}

/// @nodoc
class __$$_AppStateCopyWithImpl<$Res> extends _$NewAppStateCopyWithImpl<$Res>
    implements _$$_AppStateCopyWith<$Res> {
  __$$_AppStateCopyWithImpl(
      _$_AppState _value, $Res Function(_$_AppState) _then)
      : super(_value, (v) => _then(v as _$_AppState));

  @override
  _$_AppState get _value => super._value as _$_AppState;

  @override
  $Res call({
    Object? status = freezed,
    Object? currentUser = freezed,
    Object? currentCart = freezed,
    Object? accessToken = freezed,
  }) {
    return _then(_$_AppState(
      status: status == freezed
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as AppStatus,
      currentUser: currentUser == freezed
          ? _value.currentUser
          : currentUser // ignore: cast_nullable_to_non_nullable
              as UserNullable?,
      currentCart: currentCart == freezed
          ? _value.currentCart
          : currentCart // ignore: cast_nullable_to_non_nullable
              as Cart?,
      accessToken: accessToken == freezed
          ? _value.accessToken
          : accessToken // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_AppState implements _AppState {
  _$_AppState(
      {required this.status,
      this.currentUser,
      this.currentCart,
      this.accessToken});

  @override
  final AppStatus status;
  @override
  final UserNullable? currentUser;
  @override
  final Cart? currentCart;
  @override
  final String? accessToken;

  @override
  String toString() {
    return 'NewAppState(status: $status, currentUser: $currentUser, currentCart: $currentCart, accessToken: $accessToken)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AppState &&
            const DeepCollectionEquality().equals(other.status, status) &&
            const DeepCollectionEquality()
                .equals(other.currentUser, currentUser) &&
            const DeepCollectionEquality()
                .equals(other.currentCart, currentCart) &&
            const DeepCollectionEquality()
                .equals(other.accessToken, accessToken));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(status),
      const DeepCollectionEquality().hash(currentUser),
      const DeepCollectionEquality().hash(currentCart),
      const DeepCollectionEquality().hash(accessToken));

  @JsonKey(ignore: true)
  @override
  _$$_AppStateCopyWith<_$_AppState> get copyWith =>
      __$$_AppStateCopyWithImpl<_$_AppState>(this, _$identity);
}

abstract class _AppState implements NewAppState {
  factory _AppState(
      {required final AppStatus status,
      final UserNullable? currentUser,
      final Cart? currentCart,
      final String? accessToken}) = _$_AppState;

  @override
  AppStatus get status;
  @override
  UserNullable? get currentUser;
  @override
  Cart? get currentCart;
  @override
  String? get accessToken;
  @override
  @JsonKey(ignore: true)
  _$$_AppStateCopyWith<_$_AppState> get copyWith =>
      throw _privateConstructorUsedError;
}
