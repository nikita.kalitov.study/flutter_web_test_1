part of 'new_app_bloc.dart';

enum AppStatus {
  loading,
  loaded,
}

@freezed
class NewAppState with _$NewAppState {
  factory NewAppState({
    required AppStatus status,
    UserNullable? currentUser,
    Cart? currentCart,
    String? accessToken,
  }) = _AppState;
}
