part of 'new_app_bloc.dart';

@freezed
class NewAppEvent with _$NewAppEvent {
  const factory NewAppEvent.started() = _Loading;
  const factory NewAppEvent.pageChanged({required String route}) = _PageChanged;
  const factory NewAppEvent.goToRootPage() = _GoToRootPage;
  const factory NewAppEvent.goToUsersPage() = _GoToUsersPage;
  const factory NewAppEvent.goToSingleUserPage(
      {required UserNullable currentUser}) = _GoToSingleUserPage;
  const factory NewAppEvent.goToUserCartPage() = _GoToUserCartPage;
  const factory NewAppEvent.goToErrorPage() = _GoToErrorPage;
  const factory NewAppEvent.goToHomePage() = _GoToHomePage;
}
