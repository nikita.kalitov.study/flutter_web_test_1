// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './router.dart';
import './logic/bloc/new_app_bloc/new_app_bloc.dart';
import './logic/cubit/users_page_cubit/users_page_cubit.dart';
import './logic/cubit/single_user_page_cubit/single_user_page_cubit.dart';
import './logic/cubit/user_cart_page_cubit/user_cart_page_cubit.dart';
import './logic/cubit/home_page_cubit/home_page_cubit.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => NewAppBloc(),
        ),
        BlocProvider(
          create: (context) => UsersPageCubit(),
        ),
        BlocProvider(
          create: (context) => SingleUserPageCubit(),
        ),
        BlocProvider(
          create: (context) => UserCartPageCubit(),
        ),
        BlocProvider(
          create: (context) => HomePageCubit(),
        ),
      ],
      child: MaterialApp.router(
        debugShowCheckedModeBanner: false,
        routeInformationParser: router.routeInformationParser,
        routeInformationProvider: router.routeInformationProvider,
        routerDelegate: router.routerDelegate,
      ),
    );
  }
}
