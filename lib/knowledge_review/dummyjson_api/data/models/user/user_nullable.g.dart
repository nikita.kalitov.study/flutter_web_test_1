// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_nullable.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UserNullable _$$_UserNullableFromJson(Map<String, dynamic> json) =>
    _$_UserNullable(
      id: json['id'] as int?,
      firstName: json['firstName'] as String?,
      lastName: json['lastName'] as String?,
      age: json['age'] as int?,
      gender: json['gender'] as String?,
      username: json['username'] as String?,
      password: json['password'] as String?,
      image: json['image'] as String?,
      email: json['email'] as String?,
      token: json['token'] as String?,
    );

Map<String, dynamic> _$$_UserNullableToJson(_$_UserNullable instance) =>
    <String, dynamic>{
      'id': instance.id,
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'age': instance.age,
      'gender': instance.gender,
      'username': instance.username,
      'password': instance.password,
      'image': instance.image,
      'email': instance.email,
      'token': instance.token,
    };
