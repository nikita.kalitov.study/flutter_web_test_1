// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_nullable.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UserNullable _$UserNullableFromJson(Map<String, dynamic> json) {
  return _UserNullable.fromJson(json);
}

/// @nodoc
mixin _$UserNullable {
  int? get id => throw _privateConstructorUsedError;
  String? get firstName => throw _privateConstructorUsedError;
  String? get lastName => throw _privateConstructorUsedError;
  int? get age => throw _privateConstructorUsedError;
  String? get gender => throw _privateConstructorUsedError;
  String? get username => throw _privateConstructorUsedError;
  String? get password => throw _privateConstructorUsedError;
  String? get image => throw _privateConstructorUsedError;
  String? get email => throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserNullableCopyWith<UserNullable> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserNullableCopyWith<$Res> {
  factory $UserNullableCopyWith(
          UserNullable value, $Res Function(UserNullable) then) =
      _$UserNullableCopyWithImpl<$Res>;
  $Res call(
      {int? id,
      String? firstName,
      String? lastName,
      int? age,
      String? gender,
      String? username,
      String? password,
      String? image,
      String? email,
      String? token});
}

/// @nodoc
class _$UserNullableCopyWithImpl<$Res> implements $UserNullableCopyWith<$Res> {
  _$UserNullableCopyWithImpl(this._value, this._then);

  final UserNullable _value;
  // ignore: unused_field
  final $Res Function(UserNullable) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? age = freezed,
    Object? gender = freezed,
    Object? username = freezed,
    Object? password = freezed,
    Object? image = freezed,
    Object? email = freezed,
    Object? token = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      age: age == freezed
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_UserNullableCopyWith<$Res>
    implements $UserNullableCopyWith<$Res> {
  factory _$$_UserNullableCopyWith(
          _$_UserNullable value, $Res Function(_$_UserNullable) then) =
      __$$_UserNullableCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? id,
      String? firstName,
      String? lastName,
      int? age,
      String? gender,
      String? username,
      String? password,
      String? image,
      String? email,
      String? token});
}

/// @nodoc
class __$$_UserNullableCopyWithImpl<$Res>
    extends _$UserNullableCopyWithImpl<$Res>
    implements _$$_UserNullableCopyWith<$Res> {
  __$$_UserNullableCopyWithImpl(
      _$_UserNullable _value, $Res Function(_$_UserNullable) _then)
      : super(_value, (v) => _then(v as _$_UserNullable));

  @override
  _$_UserNullable get _value => super._value as _$_UserNullable;

  @override
  $Res call({
    Object? id = freezed,
    Object? firstName = freezed,
    Object? lastName = freezed,
    Object? age = freezed,
    Object? gender = freezed,
    Object? username = freezed,
    Object? password = freezed,
    Object? image = freezed,
    Object? email = freezed,
    Object? token = freezed,
  }) {
    return _then(_$_UserNullable(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      firstName: firstName == freezed
          ? _value.firstName
          : firstName // ignore: cast_nullable_to_non_nullable
              as String?,
      lastName: lastName == freezed
          ? _value.lastName
          : lastName // ignore: cast_nullable_to_non_nullable
              as String?,
      age: age == freezed
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int?,
      gender: gender == freezed
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      username: username == freezed
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String?,
      password: password == freezed
          ? _value.password
          : password // ignore: cast_nullable_to_non_nullable
              as String?,
      image: image == freezed
          ? _value.image
          : image // ignore: cast_nullable_to_non_nullable
              as String?,
      email: email == freezed
          ? _value.email
          : email // ignore: cast_nullable_to_non_nullable
              as String?,
      token: token == freezed
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UserNullable implements _UserNullable {
  const _$_UserNullable(
      {this.id,
      this.firstName,
      this.lastName,
      this.age,
      this.gender,
      this.username,
      this.password,
      this.image,
      this.email,
      this.token});

  factory _$_UserNullable.fromJson(Map<String, dynamic> json) =>
      _$$_UserNullableFromJson(json);

  @override
  final int? id;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final int? age;
  @override
  final String? gender;
  @override
  final String? username;
  @override
  final String? password;
  @override
  final String? image;
  @override
  final String? email;
  @override
  final String? token;

  @override
  String toString() {
    return 'UserNullable(id: $id, firstName: $firstName, lastName: $lastName, age: $age, gender: $gender, username: $username, password: $password, image: $image, email: $email, token: $token)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserNullable &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.firstName, firstName) &&
            const DeepCollectionEquality().equals(other.lastName, lastName) &&
            const DeepCollectionEquality().equals(other.age, age) &&
            const DeepCollectionEquality().equals(other.gender, gender) &&
            const DeepCollectionEquality().equals(other.username, username) &&
            const DeepCollectionEquality().equals(other.password, password) &&
            const DeepCollectionEquality().equals(other.image, image) &&
            const DeepCollectionEquality().equals(other.email, email) &&
            const DeepCollectionEquality().equals(other.token, token));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(firstName),
      const DeepCollectionEquality().hash(lastName),
      const DeepCollectionEquality().hash(age),
      const DeepCollectionEquality().hash(gender),
      const DeepCollectionEquality().hash(username),
      const DeepCollectionEquality().hash(password),
      const DeepCollectionEquality().hash(image),
      const DeepCollectionEquality().hash(email),
      const DeepCollectionEquality().hash(token));

  @JsonKey(ignore: true)
  @override
  _$$_UserNullableCopyWith<_$_UserNullable> get copyWith =>
      __$$_UserNullableCopyWithImpl<_$_UserNullable>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserNullableToJson(
      this,
    );
  }
}

abstract class _UserNullable implements UserNullable {
  const factory _UserNullable(
      {final int? id,
      final String? firstName,
      final String? lastName,
      final int? age,
      final String? gender,
      final String? username,
      final String? password,
      final String? image,
      final String? email,
      final String? token}) = _$_UserNullable;

  factory _UserNullable.fromJson(Map<String, dynamic> json) =
      _$_UserNullable.fromJson;

  @override
  int? get id;
  @override
  String? get firstName;
  @override
  String? get lastName;
  @override
  int? get age;
  @override
  String? get gender;
  @override
  String? get username;
  @override
  String? get password;
  @override
  String? get image;
  @override
  String? get email;
  @override
  String? get token;
  @override
  @JsonKey(ignore: true)
  _$$_UserNullableCopyWith<_$_UserNullable> get copyWith =>
      throw _privateConstructorUsedError;
}
