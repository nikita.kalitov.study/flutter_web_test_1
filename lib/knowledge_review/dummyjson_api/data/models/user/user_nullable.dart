import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_nullable.freezed.dart';
part 'user_nullable.g.dart';

@freezed
class UserNullable with _$UserNullable {
  const factory UserNullable({
    int? id,
    String? firstName,
    String? lastName,
    int? age,
    String? gender,
    String? username,
    String? password,
    String? image,
    String? email,
    String? token,
  }) = _UserNullable;

  factory UserNullable.fromJson(Map<String, dynamic> json) =>
      _$UserNullableFromJson(json);
}
