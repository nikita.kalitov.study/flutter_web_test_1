import 'package:freezed_annotation/freezed_annotation.dart';
import '../product.dart';

part 'cart.freezed.dart';
part 'cart.g.dart';

@freezed
class Cart with _$Cart {
  const factory Cart({
    required int id,
    required List<Product> products,
    required int total,
    required int userId,
  }) = _Cart;

  factory Cart.fromJson(Map<String, dynamic> json) => _$CartFromJson(json);
}
