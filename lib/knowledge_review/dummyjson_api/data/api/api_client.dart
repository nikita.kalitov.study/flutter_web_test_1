import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import '../models/models.dart';
import './responses/responses.dart';

part 'api_client.g.dart';

@RestApi(baseUrl: 'https://dummyjson.com/')
abstract class ApiClient {
  factory ApiClient(Dio dio, {String baseUrl}) = _ApiClient;

  @GET('users')
  Future<GetUsersResponse> getUsers();

  @GET('users/{id}')
  Future<UserNullable> getUserById(@Path('id') int id);

  @GET('users/{id}/carts')
  Future<GetCartsByUserId> getCartsByUserId(@Path('id') int id);

  @PUT('users/{id}')
  Future<UserNullable> updateUser(
    @Path('id') int id,
    @Body() UserNullable user,
  );

  @POST('users/add')
  Future<UserNullable> createUser(@Body() UserNullable user);

  @POST('auth/login')
  Future<UserNullable> login(@Body() UserLogin user);

  @GET('auth/users/{id}')
  Future<UserNullable> getAuthUserById(@Path('id') int id);

  @GET('auth/users/{id}/carts')
  Future<GetCartsByUserId> getAuthCartsByUserId(@Path('id') int id);
}
