import 'package:json_annotation/json_annotation.dart';
import '../../models/user.dart';

part 'get_users.g.dart';

@JsonSerializable()
class GetUsersResponse {
  final List<UserNullable> users;

  GetUsersResponse(this.users);

  factory GetUsersResponse.fromJson(Map<String, dynamic> json) =>
      _$GetUsersResponseFromJson(json);
}
