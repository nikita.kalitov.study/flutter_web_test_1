// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_users.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetUsersResponse _$GetUsersResponseFromJson(Map<String, dynamic> json) =>
    GetUsersResponse(
      (json['users'] as List<dynamic>)
          .map((e) => UserNullable.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetUsersResponseToJson(GetUsersResponse instance) =>
    <String, dynamic>{
      'users': instance.users,
    };
