import 'package:json_annotation/json_annotation.dart';
import '../../models/user.dart';

part 'get_single_user.g.dart';

@JsonSerializable()
class GetSingleUserResponse {
  @JsonKey(name: '')
  final UserNullable user;

  GetSingleUserResponse(this.user);

  factory GetSingleUserResponse.fromJson(Map<String, dynamic> json) =>
      _$GetSingleUserResponseFromJson(json);
}
