import 'package:json_annotation/json_annotation.dart';
import '../../models/models.dart';

part 'get_cart_by_id.g.dart';

@JsonSerializable()
class GetCartsByUserId {
  final List<Cart> carts;

  GetCartsByUserId(this.carts);

  factory GetCartsByUserId.fromJson(Map<String, dynamic> json) =>
      _$GetCartsByUserIdFromJson(json);
}
