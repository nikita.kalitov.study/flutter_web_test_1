// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_cart_by_id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetCartsByUserId _$GetCartsByUserIdFromJson(Map<String, dynamic> json) =>
    GetCartsByUserId(
      (json['carts'] as List<dynamic>)
          .map((e) => Cart.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$GetCartsByUserIdToJson(GetCartsByUserId instance) =>
    <String, dynamic>{
      'carts': instance.carts,
    };
