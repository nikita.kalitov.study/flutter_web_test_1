// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_single_user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetSingleUserResponse _$GetSingleUserResponseFromJson(
        Map<String, dynamic> json) =>
    GetSingleUserResponse(
      UserNullable.fromJson(json[''] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GetSingleUserResponseToJson(
        GetSingleUserResponse instance) =>
    <String, dynamic>{
      '': instance.user,
    };
