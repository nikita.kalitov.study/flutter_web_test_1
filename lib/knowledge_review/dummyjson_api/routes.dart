// ignore_for_file: constant_identifier_names

const RootPageRoute = '/';
const UsersPageRoute = '/users';
const SingleUserPageRoute = '/single_user';
const UserCartPageRoute = '/user_cart';
const HomePageRoute = '/home_page';
const ErrorPageRoute = '/error_page';

const RootPageName = 'Корень';
const UsersPageName = 'Пользователи';
const SingleUserPageName = 'Пользователь';
const UserCartPageName = 'Корзина';
const HomePageName = 'Домашняя страница';
const ErrorPageName = 'Ошибка';
