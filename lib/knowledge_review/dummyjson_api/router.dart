// ignore_for_file: prefer_const_constructors

import 'package:go_router/go_router.dart';
import './interface/pages/pages.dart';
import './routes.dart';

final router = GoRouter(
  // initialLocation: UsersPageRoute,
  // initialLocation: RootPageRoute,
  initialLocation: HomePageRoute,
  routes: [
    GoRoute(
      path: RootPageRoute,
      builder: (context, state) {
        return RootPage();
      },
    ),
    GoRoute(
      path: UsersPageRoute,
      builder: (context, state) {
        return UsersPage();
      },
    ),
    GoRoute(
      path: SingleUserPageRoute,
      builder: (context, state) {
        return SingleUserPage();
      },
    ),
    GoRoute(
      path: UserCartPageRoute,
      builder: (context, state) {
        return UserCartPage();
      },
    ),
    GoRoute(
      path: HomePageRoute,
      builder: (context, state) {
        return HomePage();
      },
    ),
  ],
  errorBuilder: (context, state) {
    return ErrorPage(error: state.error);
  },
);
