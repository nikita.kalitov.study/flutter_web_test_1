import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
  //приватный конструктор
  const User._();

  const factory User({
    required String name,
    int? age,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  //чтобы свой кастомный метод работал, необходимо написать приватный конструктор
  String infoUser() {
    return 'My name is $name and I am $age years old';
  }
}
