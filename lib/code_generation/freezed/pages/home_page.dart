// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import '../models/user.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    testFunction();
    return Scaffold(
      body: Center(),
    );
  }

  void testFunction() {
    User userA = User(name: 'John', age: 25);
    print(userA);
    User userB = userA.copyWith(name: 'Bob');
    print(userB);
    final json = userA.toJson();
    print(json);
    User userC = User.fromJson({'name': 'Kate', 'age': 30});
    print(userC);
    bool value = userA == userB;
    print(value);
    print(userA.infoUser());
  }
}
