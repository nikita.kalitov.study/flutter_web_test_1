import 'package:json_annotation/json_annotation.dart';
import '../user.dart';
import '../meta.dart';
part 'get_users.g.dart';

@JsonSerializable()
class GetUsersResponse {
  final Meta meta;
  final List<User> data;

  GetUsersResponse(this.meta, this.data);

  factory GetUsersResponse.fromJson(Map<String, dynamic> json) =>
      _$GetUsersResponseFromJson(json);
}
