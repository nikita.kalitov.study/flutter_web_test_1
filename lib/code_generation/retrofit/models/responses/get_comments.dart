import 'package:json_annotation/json_annotation.dart';
import '../comment.dart';
import '../meta.dart';
import '../user.dart';

part 'get_comments.g.dart';

@JsonSerializable()
class GetCommentsResponse {
  final List<Comment> data;

  GetCommentsResponse(this.data);

  factory GetCommentsResponse.fromJson(Map<String, dynamic> json) =>
      _$GetCommentsResponseFromJson(json);
}
