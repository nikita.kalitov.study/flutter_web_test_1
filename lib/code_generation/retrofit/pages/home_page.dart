// ignore_for_file: avoid_print, prefer_const_constructors

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import '../client/client.dart';
import '../models/responses/responses.dart';
import '../models/models.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final Dio dio = Dio(
    BaseOptions(
      headers: {
        'Authorization':
            'Bearer d24b71e1793e48ee4b978fc50fd143936cd070e8fc6c0e5722bf5a870c410e10',
        'Content-Type': 'application/json',
      },
    ),
  );

  bool isLoading = true;
  List<Card> list = [];

  @override
  void initState() {
    super.initState();
    // getAllUsers();
    updateUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : ListView(
                children: list,
              ),
      ),
      // body: Center(
      //   child: ElevatedButton(
      //     onPressed: () async {
      //       final GetUsersResponse response = await APIClient(Dio()).getUsers();
      //       List<User> list = response.data;
      //       print(list[2].email);
      //     },
      //     child: Text('Press'),
      //   ),
      // ),
    );
  }

  void updateUser() async {
    const int id = 3295;
    User newUser = User(
      id,
      "Fr. GtanjaAcharya",
      "geetanjali_afr@h-morissette.co",
      "female",
      "active",
    );
    final response = await APIClient(dio
            // Dio(
            //   BaseOptions(
            //     headers: {
            //       'Authorization':
            //           'Bearer d24b71e1793e48ee4b978fc50fd143936cd070e8fc6c0e5722bf5a870c410e10',
            //       'Content-Type': 'application/json',
            //     },
            //   ),
            // ),
            )
        .updateUser(id.toString(), newUser);
    User user = response.data;
    print(user.name);
  }

  void getAllUsers() async {
    List<Card> listOfCards = [];
    final GetUsersResponse response = await APIClient(Dio()).getUsers();
    List<User> listOfUsers = response.data;
    for (int i = 0; i < listOfUsers.length; i++) {
      Card card = Card(
        child: Text(
            'Name: ${listOfUsers[i].name}\nEmail: ${listOfUsers[i].email}'),
      );
      print('Name:${listOfUsers[i].name}/nEmail${listOfUsers[i].email}');
      listOfCards.add(card);
    }
    setState(() {
      list = listOfCards;
      isLoading = false;
    });
  }
}
