import 'dart:io';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart' as retrofit;
import 'package:retrofit/retrofit.dart';
import '../models/responses/get_comments.dart';
import '../models/responses/get_users.dart';
import '../models/responses/get_user.dart';
import '../models/user.dart';
part 'client.g.dart';

const key =
    'Bearer d24b71e1793e48ee4b978fc50fd143936cd070e8fc6c0e5722bf5a870c410e10';

@RestApi(baseUrl: 'https://gorest.co.in/public/v1')
abstract class APIClient {
  factory APIClient(Dio dio, {String baseUrl}) = _APIClient;

  @GET('/users')
  Future<GetUsersResponse> getUsers();

  @GET('/users/{id}')
  Future<GetUserResponse> getUserWithId(@Path('id') int id);

  @GET('/comments')
  Future<GetCommentsResponse> getPostComments(@Query('post_id') int postId);

  @GET('/comments')
  Future<GetCommentsResponse> getPostCommentsWithQueryMap(
    @Queries() Map<String, dynamic> queries,
  );

  @POST('/users')
  @retrofit.Headers(<String, dynamic>{HttpHeaders.authorizationHeader: key})
  Future<GetUserResponse> createUser(@Body() User user);

  @PUT('/users/{id}')
  // @retrofit.Headers(<String, dynamic>{HttpHeaders.authorizationHeader: key})
  Future<GetUserResponse> updateUser(
    @Path() String id,
    @Body() User user,
  );

  @DELETE('/users/{id}')
  @retrofit.Headers(<String, dynamic>{HttpHeaders.authorizationHeader: key})
  Future<void> deleteUser(@Path('id') int id);
}
