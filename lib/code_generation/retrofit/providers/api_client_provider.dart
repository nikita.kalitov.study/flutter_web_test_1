// ignore_for_file: avoid_print

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:dio/dio.dart';
import '../client/client.dart';
import '../models/user.dart';
import '../models/responses/get_users.dart';

final apiClientProvider = Provider<APIClient>(
  (ref) => APIClient(
    Dio(
      BaseOptions(contentType: 'application/json'),
    ),
  ),
);

final usersProvider = FutureProvider.autoDispose<List<User>>(
  (ref) async {
    final APIClient client = ref.watch(apiClientProvider);
    try {
      GetUsersResponse response = await client.getUsers();
      return response.data;
    } catch (e) {
      print(e);
    }
    return <User>[];
  },
);

final selectedUserProvider =
    StateNotifierProvider<SelectedUser, int>((ref) => SelectedUser());

class SelectedUser extends StateNotifier<int> {
  SelectedUser() : super(-1);

  void update(int id) => state = id;
}

final userProvider = FutureProvider(
  (ref) async {
    final APIClient client = ref.watch(apiClientProvider);
    return await client.getUserWithId(ref.watch(selectedUserProvider));
  },
);
