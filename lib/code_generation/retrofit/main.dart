// ignore_for_file: prefer_const_constructors, avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import './providers/api_client_provider.dart';
import './pages/home_page.dart' as home;
import './models/user.dart';
import 'models/comment.dart';
import 'models/responses/get_comments.dart';
import 'models/responses/get_user.dart';

void main() {
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: home.HomePage(),
    );
  }
}

class HomePage extends ConsumerWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: Column(
        // ignore: prefer_const_literals_to_create_immutables
        children: [
          // SelectedUserView(),
          ActionButtonsRow(),
          UsersListView(),
        ],
      ),
    );
  }
}

class SelectedUserView extends ConsumerWidget {
  const SelectedUserView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(userProvider).when(
      data: (userResponse) {
        User user = userResponse.data;
        return Row(
          children: [
            Text(user.email),
          ],
        );
      },
      loading: () {
        return CircularProgressIndicator();
      },
      error: (error, stackTrace) {
        return Center(
          child: Text('$error'),
        );
      },
    );
  }
}

class UsersListView extends ConsumerWidget {
  const UsersListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(usersProvider).when(
      data: (usersList) {
        return Expanded(
          child: ListView.separated(
            itemCount: usersList.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  ref
                      .watch(selectedUserProvider.notifier)
                      .update(usersList[index].id);
                },
                child: Card(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Text(usersList[index].email),
                          Text(usersList[index].name),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return SizedBox(height: 10);
            },
          ),
        );
      },
      loading: () {
        return CircularProgressIndicator();
      },
      error: (error, stackTrace) {
        return Center(
          child: Text('$error'),
        );
      },
    );
  }
}

class ActionButtonsRow extends ConsumerWidget {
  const ActionButtonsRow({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Row(
      children: [
        MaterialButton(
          onPressed: () async {
            try {
              GetCommentsResponse resp =
                  await ref.watch(apiClientProvider).getPostComments(11);
              List<Comment> comments = resp.data;
              for (var comment in comments) {
                print('Comment : ${comment.body}');
              }
            } catch (e) {
              print(e.toString());
            }
          },
          child: Text('Get Comments'),
        ),
        SizedBox(width: 5),
        MaterialButton(
          onPressed: () async {
            try {
              GetCommentsResponse resp = await ref
                  .watch(apiClientProvider)
                  .getPostCommentsWithQueryMap({
                'post_id': 3,
                'email': 'deeptimay_iyengar@kautzer.org',
              });
              List<Comment> comments = resp.data;
              for (var comment in comments) {
                print('Comment : ${comment.body}');
              }
            } catch (e) {
              print(e);
            }
          },
          child: Text('Get Comments with Q map'),
        ),
        SizedBox(width: 5),
        MaterialButton(
          onPressed: () async {
            try {
              GetUserResponse resp =
                  await ref.watch(apiClientProvider).createUser(
                        // await context.read(apiClientProvider).createUser(
                        User(
                          1,
                          'Chris',
                          'chris@gmail.com',
                          'male',
                          'active',
                        ),
                      );
              print(resp.data.toJson());
            } catch (e) {
              print(e);
            }
          },
          child: Text('Create User'),
        ),
        SizedBox(width: 5),
        MaterialButton(
          onPressed: () async {
            try {
              GetUserResponse resp =
                  await ref.watch(apiClientProvider).updateUser(
                        '1424',
                        User(
                          1424,
                          'chris bl',
                          'chris@gmail.com',
                          'male',
                          'active',
                        ),
                      );
              print(resp.data.toJson());
            } catch (e) {
              print(e);
            }
          },
          child: Text('Update User'),
        ),
        SizedBox(width: 5),
        MaterialButton(
          onPressed: () async {
            try {
              await ref
                  .watch(apiClientProvider)
                  .deleteUser(1424)
                  .then((value) => print('success deleting user'));
            } catch (e) {
              print(e);
            }
          },
          child: Text('Delete User'),
        ),
        SizedBox(width: 5),
      ],
    );
  }
}
