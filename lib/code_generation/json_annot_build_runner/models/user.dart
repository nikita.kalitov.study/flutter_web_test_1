import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final int id;

  //в случае, если в json нет такого поля, мы ошибку затыкаем этим способом
  @JsonKey(defaultValue: 'Тут пусто')
  final String name;

  //если названия переменной в классе и в json отличаются, можно сделать так
  @JsonKey(name: 'username')
  final String userName;

  final String email;
  final UserAddress address;

  User({
    required this.id,
    required this.name,
    required this.userName,
    required this.email,
    required this.address,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return _$UserFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$UserToJson(this);
  }
}

@JsonSerializable()
class UserAddress {
  final String street;
  final String zipcode;
  final String city;
  final String suite;

  UserAddress({
    required this.street,
    required this.zipcode,
    required this.city,
    required this.suite,
  });

  factory UserAddress.fromJson(Map<String, dynamic> json) {
    return _$UserAddressFromJson(json);
  }

  Map<String, dynamic> toJson() {
    return _$UserAddressToJson(this);
  }
}
