// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:flutter/material.dart';
import '../models/models.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    testFunction();
    return Scaffold(
      body: Center(
        child: Text('Text'),
      ),
    );
  }

  void testFunction() {
    Worker workerA1 = Worker(name: 'Bob', age: 25, experience: 3);
    Worker workerB1 = Worker(name: 'Rob', age: 36, experience: 7);
    Worker workerC1 = Worker(name: 'John', age: 27, experience: 6);
    List<Worker> listOfWorkers1 = [workerA1, workerB1, workerC1];
    Group group1 = Group(listOfWorkers: listOfWorkers1, groupRating: 4);

    Worker workerA2 = Worker(name: 'Hannah', age: 25, experience: 3);
    Worker workerB2 = Worker(name: 'Ann', age: 36, experience: 7);
    Worker workerC2 = Worker(name: 'Kate', age: 27, experience: 6);
    List<Worker> listOfWorkers2 = [workerA2, workerB2, workerC2];
    Group group2 = Group(listOfWorkers: listOfWorkers2, groupRating: 5);

    Worker workerA3 = Worker(name: 'Kyle', age: 25, experience: 3);
    Worker workerB3 = Worker(age: 36, experience: 7);
    Worker workerC3 = Worker(age: 27, experience: 6);
    List<Worker> listOfWorkers3 = [workerA3, workerB3, workerC3];
    Group group3 = Group(listOfWorkers: listOfWorkers3, groupRating: 4);

    List<Group> listOfGroups = [group1, group2, group3];
    Company company1 = Company(listOfGroups: listOfGroups, companyRating: 8);

    print(company1.listOfGroups[1].listOfWorkers[0].name);
    company1.listOfGroups[1].listOfWorkers = listOfWorkers1;
    print(company1.listOfGroups[1].listOfWorkers[0].name);

    Company company2 =
        company1.copyWith(listOfGroups: [group3, group3, group3]);
    print(company2.listOfGroups[1].listOfWorkers[0].name);

    final json = company1.toJson();
    print(jsonEncode(json));
  }
}
