import 'package:freezed_annotation/freezed_annotation.dart';
import '../worker.dart';

part 'group.freezed.dart';
part 'group.g.dart';

@unfreezed
class Group with _$Group {
  factory Group({
    @JsonKey(name: 'list_of_workers') required List<Worker> listOfWorkers,
    @JsonKey(name: 'group_rating') required int groupRating,
  }) = _Group;

  factory Group.fromJson(Map<String, dynamic> json) => _$GroupFromJson(json);
}
