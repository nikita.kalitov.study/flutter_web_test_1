// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'group.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Group _$GroupFromJson(Map<String, dynamic> json) {
  return _Group.fromJson(json);
}

/// @nodoc
mixin _$Group {
  @JsonKey(name: 'list_of_workers')
  List<Worker> get listOfWorkers => throw _privateConstructorUsedError;
  @JsonKey(name: 'list_of_workers')
  set listOfWorkers(List<Worker> value) => throw _privateConstructorUsedError;
  @JsonKey(name: 'group_rating')
  int get groupRating => throw _privateConstructorUsedError;
  @JsonKey(name: 'group_rating')
  set groupRating(int value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $GroupCopyWith<Group> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GroupCopyWith<$Res> {
  factory $GroupCopyWith(Group value, $Res Function(Group) then) =
      _$GroupCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'list_of_workers') List<Worker> listOfWorkers,
      @JsonKey(name: 'group_rating') int groupRating});
}

/// @nodoc
class _$GroupCopyWithImpl<$Res> implements $GroupCopyWith<$Res> {
  _$GroupCopyWithImpl(this._value, this._then);

  final Group _value;
  // ignore: unused_field
  final $Res Function(Group) _then;

  @override
  $Res call({
    Object? listOfWorkers = freezed,
    Object? groupRating = freezed,
  }) {
    return _then(_value.copyWith(
      listOfWorkers: listOfWorkers == freezed
          ? _value.listOfWorkers
          : listOfWorkers // ignore: cast_nullable_to_non_nullable
              as List<Worker>,
      groupRating: groupRating == freezed
          ? _value.groupRating
          : groupRating // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_GroupCopyWith<$Res> implements $GroupCopyWith<$Res> {
  factory _$$_GroupCopyWith(_$_Group value, $Res Function(_$_Group) then) =
      __$$_GroupCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'list_of_workers') List<Worker> listOfWorkers,
      @JsonKey(name: 'group_rating') int groupRating});
}

/// @nodoc
class __$$_GroupCopyWithImpl<$Res> extends _$GroupCopyWithImpl<$Res>
    implements _$$_GroupCopyWith<$Res> {
  __$$_GroupCopyWithImpl(_$_Group _value, $Res Function(_$_Group) _then)
      : super(_value, (v) => _then(v as _$_Group));

  @override
  _$_Group get _value => super._value as _$_Group;

  @override
  $Res call({
    Object? listOfWorkers = freezed,
    Object? groupRating = freezed,
  }) {
    return _then(_$_Group(
      listOfWorkers: listOfWorkers == freezed
          ? _value.listOfWorkers
          : listOfWorkers // ignore: cast_nullable_to_non_nullable
              as List<Worker>,
      groupRating: groupRating == freezed
          ? _value.groupRating
          : groupRating // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Group implements _Group {
  _$_Group(
      {@JsonKey(name: 'list_of_workers') required this.listOfWorkers,
      @JsonKey(name: 'group_rating') required this.groupRating});

  factory _$_Group.fromJson(Map<String, dynamic> json) =>
      _$$_GroupFromJson(json);

  @override
  @JsonKey(name: 'list_of_workers')
  List<Worker> listOfWorkers;
  @override
  @JsonKey(name: 'group_rating')
  int groupRating;

  @override
  String toString() {
    return 'Group(listOfWorkers: $listOfWorkers, groupRating: $groupRating)';
  }

  @JsonKey(ignore: true)
  @override
  _$$_GroupCopyWith<_$_Group> get copyWith =>
      __$$_GroupCopyWithImpl<_$_Group>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_GroupToJson(
      this,
    );
  }
}

abstract class _Group implements Group {
  factory _Group(
      {@JsonKey(name: 'list_of_workers') required List<Worker> listOfWorkers,
      @JsonKey(name: 'group_rating') required int groupRating}) = _$_Group;

  factory _Group.fromJson(Map<String, dynamic> json) = _$_Group.fromJson;

  @override
  @JsonKey(name: 'list_of_workers')
  List<Worker> get listOfWorkers;
  @JsonKey(name: 'list_of_workers')
  set listOfWorkers(List<Worker> value);
  @override
  @JsonKey(name: 'group_rating')
  int get groupRating;
  @JsonKey(name: 'group_rating')
  set groupRating(int value);
  @override
  @JsonKey(ignore: true)
  _$$_GroupCopyWith<_$_Group> get copyWith =>
      throw _privateConstructorUsedError;
}
