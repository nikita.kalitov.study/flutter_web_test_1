// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Group _$$_GroupFromJson(Map<String, dynamic> json) => _$_Group(
      listOfWorkers: (json['list_of_workers'] as List<dynamic>)
          .map((e) => Worker.fromJson(e as Map<String, dynamic>))
          .toList(),
      groupRating: json['group_rating'] as int,
    );

Map<String, dynamic> _$$_GroupToJson(_$_Group instance) => <String, dynamic>{
      'list_of_workers': instance.listOfWorkers,
      'group_rating': instance.groupRating,
    };
