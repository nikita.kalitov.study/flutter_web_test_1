import 'package:freezed_annotation/freezed_annotation.dart';
import '../group.dart';

part 'company.freezed.dart';
part 'company.g.dart';

@unfreezed
class Company with _$Company {
  factory Company({
    @JsonKey(name: 'list_of_groups') required List<Group> listOfGroups,
    @JsonKey(name: 'company_rating') required int companyRating,
  }) = _Company;

  factory Company.fromJson(Map<String, dynamic> json) =>
      _$CompanyFromJson(json);
}
