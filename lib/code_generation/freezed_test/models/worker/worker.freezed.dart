// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'worker.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Worker _$WorkerFromJson(Map<String, dynamic> json) {
  return _Worker.fromJson(json);
}

/// @nodoc
mixin _$Worker {
  String? get name => throw _privateConstructorUsedError;
  int get age => throw _privateConstructorUsedError;
  int get experience => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WorkerCopyWith<Worker> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkerCopyWith<$Res> {
  factory $WorkerCopyWith(Worker value, $Res Function(Worker) then) =
      _$WorkerCopyWithImpl<$Res>;
  $Res call({String? name, int age, int experience});
}

/// @nodoc
class _$WorkerCopyWithImpl<$Res> implements $WorkerCopyWith<$Res> {
  _$WorkerCopyWithImpl(this._value, this._then);

  final Worker _value;
  // ignore: unused_field
  final $Res Function(Worker) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? age = freezed,
    Object? experience = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      age: age == freezed
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      experience: experience == freezed
          ? _value.experience
          : experience // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$$_WorkerCopyWith<$Res> implements $WorkerCopyWith<$Res> {
  factory _$$_WorkerCopyWith(_$_Worker value, $Res Function(_$_Worker) then) =
      __$$_WorkerCopyWithImpl<$Res>;
  @override
  $Res call({String? name, int age, int experience});
}

/// @nodoc
class __$$_WorkerCopyWithImpl<$Res> extends _$WorkerCopyWithImpl<$Res>
    implements _$$_WorkerCopyWith<$Res> {
  __$$_WorkerCopyWithImpl(_$_Worker _value, $Res Function(_$_Worker) _then)
      : super(_value, (v) => _then(v as _$_Worker));

  @override
  _$_Worker get _value => super._value as _$_Worker;

  @override
  $Res call({
    Object? name = freezed,
    Object? age = freezed,
    Object? experience = freezed,
  }) {
    return _then(_$_Worker(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      age: age == freezed
          ? _value.age
          : age // ignore: cast_nullable_to_non_nullable
              as int,
      experience: experience == freezed
          ? _value.experience
          : experience // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Worker implements _Worker {
  const _$_Worker(
      {this.name = 'incognito', required this.age, required this.experience});

  factory _$_Worker.fromJson(Map<String, dynamic> json) =>
      _$$_WorkerFromJson(json);

  @override
  @JsonKey()
  final String? name;
  @override
  final int age;
  @override
  final int experience;

  @override
  String toString() {
    return 'Worker(name: $name, age: $age, experience: $experience)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Worker &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.age, age) &&
            const DeepCollectionEquality()
                .equals(other.experience, experience));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(age),
      const DeepCollectionEquality().hash(experience));

  @JsonKey(ignore: true)
  @override
  _$$_WorkerCopyWith<_$_Worker> get copyWith =>
      __$$_WorkerCopyWithImpl<_$_Worker>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WorkerToJson(
      this,
    );
  }
}

abstract class _Worker implements Worker {
  const factory _Worker(
      {final String? name,
      required final int age,
      required final int experience}) = _$_Worker;

  factory _Worker.fromJson(Map<String, dynamic> json) = _$_Worker.fromJson;

  @override
  String? get name;
  @override
  int get age;
  @override
  int get experience;
  @override
  @JsonKey(ignore: true)
  _$$_WorkerCopyWith<_$_Worker> get copyWith =>
      throw _privateConstructorUsedError;
}
