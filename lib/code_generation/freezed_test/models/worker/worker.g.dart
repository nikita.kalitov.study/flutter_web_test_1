// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'worker.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Worker _$$_WorkerFromJson(Map<String, dynamic> json) => _$_Worker(
      name: json['name'] as String? ?? 'incognito',
      age: json['age'] as int,
      experience: json['experience'] as int,
    );

Map<String, dynamic> _$$_WorkerToJson(_$_Worker instance) => <String, dynamic>{
      'name': instance.name,
      'age': instance.age,
      'experience': instance.experience,
    };
