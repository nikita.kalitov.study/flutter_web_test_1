///список пользователей (имитация бд)

// ignore_for_file: avoid_print

class User {
  final String? login;
  final String? password;
  User({required this.login, required this.password});
}

class Database {
  Database();

  List<User> list = [
    User(login: 'petya@mail.ru', password: '1234'),
    User(login: 'dima@mail.ru', password: '1234'),
    User(login: 'alex@mail.ru', password: '1234'),
    User(login: 'yuri@mail.ru', password: '1234'),
  ];

  bool isLoggedIn = true;

  // bool checkUsers(String login, String password) {
  //   print('check users launched');
  //   bool value = false;
  //   for (int i = 0; i < list.length; i++) {
  //     if (list[i].login == login && list[i].password == password) {
  //       value = true;
  //       break;
  //     } else {
  //       value = false;
  //     }
  //   }
  //   return value;
  // }

  // void changeLogStatus(bool value) {
  //   isLoggedIn = value;
  // }
}
