const sideMenuItemHeight = 70;
const appBarHeight = 60;
const sideMenuWidth = 200;

const bigScreenWidth = 1350;
const mediumScreenWidth = 1000;
const smallScreenWidth = 570;
const minScreenSize = 800;
