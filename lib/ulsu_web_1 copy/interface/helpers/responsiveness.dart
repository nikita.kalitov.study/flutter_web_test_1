import 'package:flutter/material.dart';
import '../../data/constants/sizes.dart';

class Responsiveness {
  static bool isSmallScreen(BuildContext context) {
    return MediaQuery.of(context).size.width <= smallScreenWidth;
  }

  static bool isMediumScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > smallScreenWidth &&
        MediaQuery.of(context).size.width <= mediumScreenWidth;
  }

  static bool isBigScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > mediumScreenWidth &&
        MediaQuery.of(context).size.width <= bigScreenWidth;
  }

  static bool isLargeScreen(BuildContext context) {
    return MediaQuery.of(context).size.width > bigScreenWidth;
  }
}
