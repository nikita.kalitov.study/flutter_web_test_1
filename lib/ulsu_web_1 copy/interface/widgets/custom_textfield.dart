// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

// class CustomTextField extends StatefulWidget {
//   const CustomTextField({Key? key}) : super(key: key);

//   @override
//   State<CustomTextField> createState() => _CustomTextFieldState();
// }

// class _CustomTextFieldState extends State<CustomTextField> {
//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       height: 200,
//       width: 200,
//       child: TextField(
//         textAlign: TextAlign.center,
//       ),
//     );
//   }
// }

class CustomTextField extends StatelessWidget {
  const CustomTextField({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      width: 200,
      child: TextField(
        textAlign: TextAlign.center,
      ),
    );
  }
}
