// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import '../../router.dart';
import '../../routes.dart';
import '../../logic/providers/data_provider.dart';

class RootScreen extends StatelessWidget {
  const RootScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('RootScreen built');
    imitateWaitingAndGo();
    return const Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}

Future<void> imitateWaitingAndGo() async {
  // await Future.delayed(const Duration(seconds: 3));
  dataProvider.isLoggedIn()
      ? router.go(MainScreenRoute)
      : router.go(LoginScreenRoute);
}
