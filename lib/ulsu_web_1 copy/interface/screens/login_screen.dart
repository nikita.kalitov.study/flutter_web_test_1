// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import '../../logic/bloc/pages_bloc/pages_bloc.dart';
import '../../logic/providers/data_provider.dart';
import '../../logic/bloc/app_bloc/app_bloc.dart';
import '../../routes.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final loginController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    print('LoginScreen built');
    return BlocBuilder<AppBloc, AppState>(
      builder: (context, state) {
        print('BlocBuilder built');
        return Scaffold(
          // body: Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.center,
          //   children: [
          //     Row(
          //       children: [Text('Логин: '), TextField(),],
          //       mainAxisAlignment: MainAxisAlignment.center,
          //     ),
          //     Row(
          //       children: [Text('Пароль: ')],
          //       mainAxisAlignment: MainAxisAlignment.center,
          //     ),
          //     ElevatedButton(
          //       onPressed: () {
          //         context.read<AppBloc>().add(AppUserLoggedIn());
          //       },
          //       child: Text('Log in'),
          //     ),
          //   ],
          // ),

          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Логин:'),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    width: 200,
                    child: TextField(
                      controller: loginController,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text('Пароль:'),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    width: 200,
                    child: TextField(
                      controller: passwordController,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 50,
              ),
              BlocBuilder<PagesBloc, PagesState>(
                builder: (context, state) {
                  return BlocBuilder<AppBloc, AppState>(
                    builder: (context, state) {
                      return ElevatedButton(
                        onPressed: () {
                          GoRouter.of(context).pop();
                          // if (canLogIn(
                          //   loginController.text,
                          //   passwordController.text,
                          // )) {
                          //   context.read<AppBloc>().add(AppUserLoggedIn());
                          //   context
                          //       .read<PagesBloc>()
                          //       .add(GoToAnotherPage(path: MainScreenRoute));
                          // } else {
                          //   print(
                          //       '${loginController.text}, ${passwordController.text},');
                          //   print('you cannot log in');
                          // }
                        },
                        child: const Text('Log in'),
                      );
                    },
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }

  bool canLogIn(String login, String password) {
    return dataProvider.checkUsers(login, password);
  }
}
