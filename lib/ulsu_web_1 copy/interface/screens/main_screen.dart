// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import '../../data/constants/sizes.dart';
import '../../logic/bloc/app_bloc/app_bloc.dart';
import '../../logic/bloc/pages_bloc/pages_bloc.dart';
import '../widgets/side_menu.dart';
import '../widgets/app_bar.dart';
import '../helpers/responsiveness.dart';
import '../../routes.dart';
import '../../router.dart';

class MainScreen extends StatelessWidget {
  MainScreen({Key? key, required this.path}) : super(key: key);

  final String path;
  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    print('MainScreen built');
    return Scaffold(
      appBar: CustomAppBar('Main screen'),
      drawer: Responsiveness.isSmallScreen(context)
          ? CustomSideMenu(path: path)
          : null,
      body: MainScreenBody(
        path: path,
      ),
    );
  }
}

class MainScreenBody extends StatelessWidget {
  MainScreenBody({
    Key? key,
    required this.path,
  }) : super(key: key);

  final String path;
  final double containerWidth = 1000;
  final _horizontalScrollController = ScrollController();
  final _verticalScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    print('width: $screenWidth\nheight: $screenWidth');

    return SizedBox(
      width: screenWidth,
      height: screenHeight,
      child: Scrollbar(
        controller: _horizontalScrollController,
        thumbVisibility: true,
        child: SingleChildScrollView(
          controller: _horizontalScrollController,
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              if (!Responsiveness.isSmallScreen(context))
                CustomSideMenu(path: path),
              SizedBox(
                width: (screenWidth - sideMenuWidth) >= containerWidth
                    ? screenWidth - sideMenuWidth
                    : containerWidth,
                child: Center(
                  child: Scrollbar(
                    controller: _verticalScrollController,
                    thumbVisibility: true,
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      controller: _verticalScrollController,
                      child: Container(
                        color: Colors.grey[500],
                        width: containerWidth,
                        height: 800,
                        child: Center(
                          child: BlocBuilder<PagesBloc, PagesState>(
                            builder: (context, state) {
                              return BlocBuilder<AppBloc, AppState>(
                                builder: (context, state) {
                                  return ElevatedButton(
                                    onPressed: () {
                                      // func();
                                      GoRouter.of(context)
                                          .push(LoginScreenRoute);
                                      // context
                                      //     .read<AppBloc>()
                                      //     .add(AppUserLoggedOut());
                                      // context.read<PagesBloc>().add(
                                      //     GoToAnotherPage(
                                      //         path: LoginScreenRoute));
                                    },
                                    child: const Text('Log out'),
                                  );
                                },
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
