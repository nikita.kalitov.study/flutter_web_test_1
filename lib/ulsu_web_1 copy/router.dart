// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import './logic/providers/data_provider.dart';
import './routes.dart';
import './interface/screens/screens.dart';

final NavigatorObserver observer = NavigatorObserver();

void func() {
  // observer.didRemove(, 'previousRoute');
}

final router = GoRouter(
  initialLocation:
      dataProvider.isLoggedIn() ? MainScreenRoute : LoginScreenRoute,
  routes: [
    GoRoute(
      path: '/',
      builder: (context, state) {
        return RootScreen();
      },
    ),
    GoRoute(
      name: LoginScreenRoute,
      path: LoginScreenRoute,
      // pageBuilder: (context, state) => MaterialPage<void>(
      //   key: state.pageKey,
      //   child: LoginScreen(),
      // ),
      builder: (context, state) {
        return LoginScreen();
      },
    ),
    GoRoute(
      name: MainScreenRoute,
      path: MainScreenRoute,
      builder: (context, state) {
        return MainScreen(path: MainScreenRoute);
      },
    ),
    GoRoute(
      name: ProjectsScreenRoute,
      path: ProjectsScreenRoute,
      builder: (context, state) {
        return StackScreen(
          path: ProjectsScreenRoute,
        );
      },
    ),
    GoRoute(
      name: TeamsScreenRoute,
      path: TeamsScreenRoute,
      builder: (context, state) {
        return ServicesScreen(
          path: TeamsScreenRoute,
        );
      },
    ),
    GoRoute(
      name: InterviewScreenRoute,
      path: InterviewScreenRoute,
      builder: (context, state) {
        return SchoolsScreen(
          path: InterviewScreenRoute,
        );
      },
    ),
    GoRoute(
      name: UsersScreenRoute,
      path: UsersScreenRoute,
      builder: (context, state) {
        return PartnershipScreen(
          path: UsersScreenRoute,
        );
      },
    ),
    GoRoute(
      name: DataScreenRoute,
      path: DataScreenRoute,
      builder: (context, state) {
        return CareerScreen(
          path: DataScreenRoute,
        );
      },
    ),
  ],
  errorBuilder: (context, state) {
    return ErrorPage(error: state.error);
  },
  observers: [observer],
);
