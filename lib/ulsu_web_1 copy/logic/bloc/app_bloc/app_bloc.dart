import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
// import 'package:go_router/go_router.dart';
// import 'package:flutter_web_test_1/ulsu_web_1%20copy/data/database.dart';
// import '../../data/database.dart';
import '../../../logic/providers/data_provider.dart';
import '../../../router.dart';
import '../../../routes.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc() : super(AppInitial(dataProvider.isLoggedIn())) {
    on<AppUserLoggedIn>(_onUserLoggedIn);
    on<AppUserLoggedOut>(_onUserLoggedOut);
  }

  void _onUserLoggedIn(AppUserLoggedIn event, Emitter<AppState> emit) async {
    dataProvider.changeLogStatus(true);
    // router.pop();
    // router.go(MainScreenRoute);
    emit(AppIsLogged(dataProvider.isLoggedIn()));
  }

  void _onUserLoggedOut(AppUserLoggedOut event, Emitter<AppState> emit) async {
    dataProvider.changeLogStatus(false);
    // router.pop();
    // router.go(LoginScreenRoute);
    // router.navigator
    // ?.pushNamedAndRemoveUntil(LoginScreenName, (route) => false);
    emit(AppNotLogged(dataProvider.isLoggedIn()));
  }
}
