part of 'app_bloc.dart';

abstract class AppEvent extends Equatable {
  const AppEvent();

  @override
  List<Object> get props => [];
}

class AppUserLoggedIn extends AppEvent {
  const AppUserLoggedIn();

  @override
  List<Object> get props => [];
}

class AppUserLoggedOut extends AppEvent {
  const AppUserLoggedOut();

  @override
  List<Object> get props => [];
}
