part of 'app_bloc.dart';

abstract class AppState extends Equatable {
  final bool isUserLoggedIn;

  const AppState(this.isUserLoggedIn);

  @override
  List<Object> get props => [isUserLoggedIn];
}

class AppInitial extends AppState {
  const AppInitial(bool value) : super(value);
}

class AppNotLogged extends AppState {
  const AppNotLogged(bool value) : super(value);

  @override
  List<Object> get props => [];
}

class AppIsLogged extends AppState {
  const AppIsLogged(bool value) : super(value);

  @override
  List<Object> get props => [];
}
