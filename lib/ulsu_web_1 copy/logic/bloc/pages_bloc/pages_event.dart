part of 'pages_bloc.dart';

abstract class PagesEvent extends Equatable {
  const PagesEvent();

  @override
  List<Object> get props => [];
}

// class GoToMainPage extends PagesEvent {
//   final String path;
//   const GoToMainPage({required this.path});

//   @override
//   List<Object> get props => [];
// }

// class GoToProjectsPage extends PagesEvent {}

// class GoToTeamsPage extends PagesEvent {}

// class GoToInterviewPage extends PagesEvent {}

// class GoToUsersPage extends PagesEvent {}

// class GoToDataPage extends PagesEvent {}

class GoToAnotherPage extends PagesEvent {
  final String path;
  const GoToAnotherPage({required this.path});

  @override
  List<Object> get props => [path];
}
