part of 'pages_bloc.dart';

abstract class PagesState extends Equatable {
  const PagesState();

  @override
  List<Object> get props => [];
}

class PagesInitial extends PagesState {}

class RootPageState extends PagesState {}

class LoginPageState extends PagesState {}

class MainPageState extends PagesState {}

class ProjectsPageState extends PagesState {}

class TeamsPageState extends PagesState {}

class InterviewPageState extends PagesState {}

class UsersPageState extends PagesState {}

class DataPageState extends PagesState {}
