// ignore_for_file: avoid_print

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import '../../../router.dart';
import '../../../routes.dart';

part 'pages_event.dart';
part 'pages_state.dart';

class PagesBloc extends Bloc<PagesEvent, PagesState> {
  PagesBloc() : super(PagesInitial()) {
    on<GoToAnotherPage>(_onGoToAnotherPage);
    // on<GoToMainPage>(_onGoToMainPage);
    // on<GoToProjectsPage>(_onGoToProjectsPage);
    // on<GoToTeamsPage>(_onGoToTeamsPage);
    // on<GoToInterviewPage>(_onGoToInterviewPage);
    // on<GoToUsersPage>(_onGoToUsersPage);
    // on<GoToDataPage>(_onGoToDataPage);
  }

  void _onGoToAnotherPage(
      GoToAnotherPage event, Emitter<PagesState> emit) async {
    router.go(event.path);
    router.routeInformationProvider.didPopRoute();
    print('router.location');

    switch (event.path) {
      case RootScreenRoute:
        emit(RootPageState());
        print('emitted RootPageState');
        break;
      case LoginScreenRoute:
        emit(LoginPageState());
        print('emitted LoginPageState');
        break;
      case MainScreenRoute:
        emit(MainPageState());
        print('emitted MainPageState');
        break;
      case ProjectsScreenRoute:
        emit(ProjectsPageState());
        print('emitted ProjectsPageState');
        break;
      case TeamsScreenRoute:
        emit(TeamsPageState());
        print('emitted TeamsPageState');
        break;
      case InterviewScreenRoute:
        emit(InterviewPageState());
        print('emitted InterviewPageState');
        break;
      case UsersScreenRoute:
        emit(UsersPageState());
        print('emitted UsersPageState');
        break;
      case DataScreenRoute:
        emit(DataPageState());
        print('emitted DataPageState');
        break;
    }
  }

  // void _onGoToMainPage(GoToMainPage event, Emitter<PagesState> emit) async {
  // }

  // void _onGoToProjectsPage(
  //     GoToProjectsPage event, Emitter<PagesState> emit) async {}

  // void _onGoToTeamsPage(GoToTeamsPage event, Emitter<PagesState> emit) async {}

  // void _onGoToInterviewPage(
  //     GoToInterviewPage event, Emitter<PagesState> emit) async {}

  // void _onGoToUsersPage(GoToUsersPage event, Emitter<PagesState> emit) async {}

  // void _onGoToDataPage(GoToDataPage event, Emitter<PagesState> emit) async {}
}
