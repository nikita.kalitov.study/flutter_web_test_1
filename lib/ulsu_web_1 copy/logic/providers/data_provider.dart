// ignore_for_file: avoid_print

import '../../data/database.dart';

// Database databaseProvider = Database();

DataProvider dataProvider = DataProvider();

class DataProvider {
  final Database _database = Database();

  List<User> _getListOfUsers() {
    print('got list of users');
    return _database.list;
  }

  bool isLoggedIn() {
    print('isLoggedIn launched');
    return _database.isLoggedIn;
  }

  bool checkUsers(String login, String password) {
    print('checkUsers launched');
    final list = _getListOfUsers();
    bool value = false;
    for (int i = 0; i < list.length; i++) {
      if (list[i].login == login && list[i].password == password) {
        value = true;
        break;
      } else {
        value = false;
      }
    }
    return value;
  }

  void changeLogStatus(bool value) {
    print('changeLogStatus launched');
    _database.isLoggedIn = value;
  }
}
