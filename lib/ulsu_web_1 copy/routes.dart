// ignore_for_file: constant_identifier_names

const RootScreenRoute = '/';
const LoginScreenRoute = '/login';
const MainScreenRoute = '/main';
const ProjectsScreenRoute = '/projects';
const TeamsScreenRoute = '/teams';
const InterviewScreenRoute = '/interview';
const UsersScreenRoute = '/users';
const DataScreenRoute = '/data';

const RootScreenName = 'Корень';
const LoginScreenName = 'Логин';
const MainScreenName = 'Главная';
const ProjectsScreenName = 'Проекты';
const TeamsScreenName = 'Команды';
const InterviewScreenName = 'Собеседование';
const UsersScreenName = 'Пользователи';
const DataScreenName = 'Данные системы';
