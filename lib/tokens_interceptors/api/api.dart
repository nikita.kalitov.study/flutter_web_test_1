import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class Api {
  Dio dio = Dio();
  String? accessToken;

  final _storage = const FlutterSecureStorage();

  Api() {
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: ((options, handler) async {
        if (!options.path.contains('http')) {
          options.path = 'http://temp1.ulsu.ru/api/v3/${options.path}';
        }
        options.headers['Authorization'] = 'Bearer $accessToken';
        return handler.next(options);
      }),
      onError: (error, handler) async {
        if (error.response?.statusCode == 401) {
          if (await _storage.containsKey(key: 'refreshToken')) {
            await refreshToken();
            return handler.resolve(await _retry(error.requestOptions));
          }
        }
        return handler.next(error);
      },
    ));
  }

  void testFunc() async {
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        return handler.next(options);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: (error, handler) {
        return handler.next(error);
      },
    ));
  }

  Future<Dio> getApiClient() async {
    Dio dio = Dio();
    String token = '';
    dio.interceptors.clear();
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) {
        return handler.next(options);
      },
      onResponse: (response, handler) {
        return handler.next(response);
      },
      onError: (error, handler) {
        return handler.next(error);
      },
    ));
    return dio;
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return dio.request<dynamic>(
      requestOptions.path,
      data: requestOptions.data,
      queryParameters: requestOptions.queryParameters,
      options: options,
    );
  }

  Future<void> refreshToken() async {
    final refreshToken = await _storage.read(key: 'refreshToken');
    final response = await dio
        .post('Access/RefreshToken', data: {'refreshToken': refreshToken});

    if (response.statusCode == 201) {
      print('if выполнилось');
      accessToken = response.data;
    } else {
      accessToken == null;
      _storage.deleteAll();
    }

    // {
    // "grant_type": "moodle",
    // "username": "kalitovns@stud.ulsu.ru",
    // "password": "gyRui5O*",
    // "device": ""
    // }
  }
}
