// ignore_for_file: prefer_const_constructors, unnecessary_string_interpolations

import 'package:dio/dio.dart';
import './new_api.dart';
import 'token.dart';

Dio myDio = _createDio();

Dio _createDio() {
  int number = 0;
  Dio dio = Dio(
    BaseOptions(
      // baseUrl: 'https://jsonplaceholder.typicode.com/',
      baseUrl: 'https://dummyjson.com/',
      headers: {
        'Content-Type': 'application/json',
      },
    ),
  );
  dio.interceptors.add(InterceptorsWrapper(
    onRequest: (options, handler) async {
      /*print('onRequest before');
      print('${options.path}');
      await Future.delayed(Duration(seconds: 1));
      print('onRequest after');*/
      return handler.next(options);
    },
    onResponse: (response, handler) async {
      //   /*print('onResponse before');
      //   print('${response.requestOptions.path}');
      //   await Future.delayed(Duration(seconds: 1));
      //   print('onResponse after');*/
      return handler.next(response);
    },
    onError: (error, handler) async {
      /*print('onError before');
      print('${error.requestOptions.path}');
      await Future.delayed(Duration(seconds: 1));
      if (error.response?.statusCode == 404) {
        String newResponse = await NewApi.retry(dio, error.requestOptions);
        print('new request: $newResponse');
      } else*/
      print('print: error!');
      if ((error.response?.statusCode == 401 ||
              error.response?.statusCode == 403) &&
          (numberOfTries < 3)) {
        //refresh token with the same dio
        //dio.refreshToken();
        numberOfTries = numberOfTries + 1;
        print('number: $numberOfTries');
        await NewestApi.userLogin();
        dio.options.headers = {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $accessToken',
          // 'Authorization': 'Bearer ',
        };
        final clone = await dio.request(error.requestOptions.path,
            options: Options(
              method: error.requestOptions.method,
            ));
        // final clone = await dio.get('http/401');

        // number++;
        return handler.resolve(clone);
      }
      // numberOfTries = numberOfTries + 1;
      // print('number: $numberOfTries');
      // print('onError after');
      return handler.next(error);
    },
  ));
  return dio;
}

class MyInterceptor extends QueuedInterceptor {}
