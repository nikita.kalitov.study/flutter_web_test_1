// ignore_for_file: prefer_const_constructors

import 'package:dio/dio.dart';
import './new_api.dart';
import '../main.dart';

final Dio globalDio = _getDio();

Dio _getDio() {
  Dio dio = Dio();
  dio.interceptors.add(InterceptorsWrapper(
    onRequest: (options, handler) async {
      print('onRequest');
      await Future.delayed(Duration(seconds: 4));
      print('onRequest after delay');
      return handler.next(options);
    },
    onResponse: (response, handler) async {
      print('onResponse');
      await Future.delayed(Duration(seconds: 4));
      print('onResponse after delay');
      return handler.next(response);
    },
    onError: (error, handler) async {
      print('onError');
      await Future.delayed(Duration(seconds: 4));
      print('onError after delay');
      if (error.response?.statusCode == 401) {
        NewApi.refresh();
      }
      return handler.next(error);
    },
  ));
  return dio;
}
