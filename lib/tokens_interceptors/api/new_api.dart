// ignore_for_file: prefer_const_constructors, unnecessary_string_interpolations

import 'dart:convert';

import 'package:dio/dio.dart';

import './dio.dart';
import './my_dio.dart';
import './token.dart';

class NewestApi {
  static Future<void> userLogin() async {
    // Dio dio = myDio;
    final response = await myDio.post('auth/login', data: {
      'username': 'kminchelle',
      'password': '0lelplR',
      'expiresInMins': '0.1',
    });
    accessToken = await jsonDecode(response.toString())['token'];
    print('print: $accessToken');
    // return jsonDecode(response.toString())['token'];
  }

  static Future<String> getUser() async {
    // Dio dio = myDio;
    final response = await myDio.get('users/1');
    return response.toString();
  }

  static Future<String> getUserAuth() async {
    // Dio dio = myDio;
    if (accessToken != '') {
      myDio.options.headers.addAll({'Authorization': 'Bearer $accessToken'});
      final response = await myDio.get('auth/users/1');
      return response.toString();
    } else {
      return 'token is empty!';
    }
  }
}

class NewApi {
  static Future<String> getResponse() async {
    Dio dio = Dio(
      BaseOptions(
        baseUrl: 'https://jsonplaceholder.typicode.com/',
        headers: {
          'Content-Type': 'application/json',
        },
      ),
    );
    dio.interceptors.add(InterceptorsWrapper(
      onRequest: (options, handler) async {
        print('onRequest before');
        // print('${options.path}');
        // await Future.delayed(Duration(seconds: 1));
        print('onRequest after');
        return handler.next(options);
      },
      onResponse: (response, handler) async {
        print('onResponse before');
        // print('${response.requestOptions.path}');
        // await Future.delayed(Duration(seconds: 1));
        print('onResponse after');
        return handler.next(response);
      },
      onError: (error, handler) async {
        print('onError before');
        // print('${error.requestOptions.path}');
        // await Future.delayed(Duration(seconds: 1));
        if (error.response?.statusCode == 404) {
          String newResponse = await retry(dio, error.requestOptions);
          print('new request: $newResponse');
        }
        print('onError after');
        return handler.next(error);
      },
    ));
    String path = '/uses/2';
    final response = await dio.get(path);
    // final response = await dio.request(path, options: Options(method: 'GET'));
    return response.toString();
  }

  static Future<String> retry(Dio dio, RequestOptions requestOptions) async {
    // String path = '/use/2';
    requestOptions.path = '/users/2';
    final response = await dio.get(requestOptions.path);
    return response.toString();
    // return requestOptions.path;
  }

  static Future<String> getUser() async {
    final response = await globalDio
        .get('https://jsonplaceholder.typicode.com/s/2')
        .then((response) async {
      print('on ${response.data.toString()}');
    });
    return response.toString();
  }

  // static Future<void> retry() async {
  //   final response = await globalDio
  //       .get('https://jsonplaceholder.typicode.com/users/2')
  //       .then((response) async {
  //     print('on ${response.data.toString()}');
  //   });
  // }

  static Future<void> refresh() async {
    print('on token refreshed');
  }
}
