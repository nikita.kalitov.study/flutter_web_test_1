// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import '../api/new_api.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // NewApi.getResponse();
    printFunc();
    return Scaffold(
      body: Center(
        child: Container(
          child: Column(
            children: [
              ElevatedButton(
                onPressed: () async {
                  print('print: ${await NewestApi.getUserAuth()}');
                },
                child: Text('Get user auth'),
              ),
              ElevatedButton(
                onPressed: () async {
                  await NewestApi.userLogin();
                },
                child: Text('Refresh token'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void launchFunc() async {}

  void printFunc() async {
    await NewestApi.userLogin();
    // print(await NewApi.getResponse());
    // print(await NewestApi.userLogin());
    // String testString = 'test string is here';
    // print(testString.replaceRange(16, 17, 'a'));
  }
}
