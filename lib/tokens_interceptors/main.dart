// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import './api/dio.dart';
import './api/new_api.dart';
import './pages/home_page.dart' as pages;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: HomePage(),
      home: pages.HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // testFunc();
    // NewApi.getUser();
    newFunction();
    return Scaffold(
      body: Center(
        child: Container(),
      ),
    );
  }

  void newFunction() async {
    try {
      final response = await NewApi.getUser();
      print(response);
    } catch (error) {
      DioError dioError = error as DioError;
      print(dioError.response?.statusCode);
      newFunction();
    }
  }

  void testFunc() async {
    // final Dio dio = getDio();
    final response = await globalDio
        .get('https://jsonplaceholder.typicode.com/s/2')
        .then((response) async {
      print('on then start');
      await Future.delayed(Duration(seconds: 4));
      print('on then after delay');
      print('on response ${response.data.toString()}');
    }).whenComplete(() async {
      print('on whenComplete');
    });
  }

  static void testFunc2() async {
    // final Dio dio = getDio();
    final response = await globalDio
        .get('https://jsonplaceholder.typicode.com/users/2')
        .then((response) async {
      print('testFunc2');
      print('then start');
      await Future.delayed(Duration(seconds: 4));
      print('then after delay');
      print('response ${response.data.toString()}');
    }).whenComplete(() async {
      print('whenComplete');
    });
  }
}
