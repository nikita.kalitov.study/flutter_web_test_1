// ignore_for_file: avoid_print, prefer_const_constructors

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import '../models/user.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isLoading = false;
  List<User>? list;
  final Dio _dio = Dio();

  void getDataDio() async {
    setState(() {
      isLoading = true;
    });

    try {
      final response = await _dio.get(
        // 'https://jsonplaceholder.typicode.com/users',
        'https://be2ab3be-1c4e-40e1-9368-fef89dd588a5.mock.pstmn.io/putUserInfo/fjkernfsrklfnrfkl',
      );
      List<User> listOfUsers = getListOfUsers(response.data);
      list = listOfUsers;

      setState(() {
        isLoading = false;
      });
    } on DioError catch (e) {
      print(e.response);
    }
  }

  //оборачиваем try-catch
  void getDataHttp() async {
    setState(() {
      isLoading = true;
    });

    //try-catch
    try {
      final response = await http.get(
        Uri.parse(
          // 'https://be2ab3be-1c4e-40e1-9368-fef89dd588a5.mock.pstmn.io/putUserInfo',
          'https://jsonplaceholder.typicode.com/users',
        ),
      );
      List<dynamic> data = jsonDecode(response.body);
      List<User> listOfUsers = getListOfUsers(data);
      list = listOfUsers;

      setState(() {
        isLoading = false;
      });
    } catch (e) {
      print(e);
    }
  }

  List<User> getListOfUsers(List<dynamic> list) {
    List<User> thisList = [];
    for (int i = 0; i < list.length; i++) {
      thisList.add(User.fromJson(list[i]));
    }
    return thisList;
  }

  @override
  void initState() {
    super.initState();
    // getDataHttp();
    getDataDio();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator()
            : ListView.builder(
                itemCount: list?.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(
                      '${list?[index].name}',
                    ),
                    subtitle: Text('${list?[index].userName}'),
                  );
                },
              ),
      ),
    );
  }
}
